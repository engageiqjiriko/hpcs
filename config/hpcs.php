<?php

return [
    'max_hpcs' => 7,
    'challenge_trigger' => 7,
    'eiq_affiliate_id' => 7746,
    'cake_id' => 7746,
    'rev_tracker' => "CD7746",
    'hpcs_admin_email' => env('HPCS_ADMIN_EMAIL', 'engageiqwinston@gmail.com'),
    'send_lead_url'=>env('SEND_LEAD_URL', 'http://testleadreactor.engageiq.com/sendLead/?'),

    'encryption_key'=> env('ENCRYPTION_KEY', 'tZAbbtnR28A/e2DLepW34KxgD7HdW99OU7ENSgiyxpU='),
    'mac_key'=> env('MAC_KEY', 'vAaHtZsyaPh+DasWsB3nBWpuu6I/frxTmz0P5/7Rth4='),
    'encryption_applied'=>env('ENCRYPTION_APPLIED',false),

    'nlr_username'=> env('NLR_USERNAME', 'api@engageiq.com'),
    'nlr_password'=>env('NLR_PASSWORD','123456'),


    'site_mode'=>env('SITE_MODE','LIVE'),
];