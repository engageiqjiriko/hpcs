<?php


Route::group(['prefix' => 'admin', 'middleware' => ['web'] ],function() {
    // Authentication Routes...
    Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\AdminLoginController@login');
    Route::post('logout', 'Auth\AdminLoginController@logout');
    Route::get('logout', 'Auth\AdminLoginController@logout');
});


Route::group(['prefix' => 'admin' , 'middleware' => ['web','auth:admin'] ],function() {

    Route::get('logs',
        [
            'as' => 'admin.laravel.log',
            'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index'
        ]);
});


Route::group(['namespace' => 'Backend' , 'middleware' => ['web'] ],function() {


    Route::post('review', [
        'as' => 'review.store',
        'uses' => 'ReviewController@store'
    ]);

    Route::group(['prefix' => 'admin' , 'middleware' => ['web'] ],function() {

        Route::get('dashboard', 'DashboardController@index');


//        NEW HPCS ROUTE
        Route::get('user', 'UserController@index');
        Route::get('user/data', 'UserController@data');
        Route::get('user/lists', 'UserController@lists');
        Route::patch('user/update', 'UserController@update');
        Route::patch('user/send/payment', 'UserController@sendPayment');


//        OLD HPCS ROUTE
        Route::get('user/lists/old', 'UserController@oldLists');
        Route::get('user/profile/{id}/old', 'UserController@oldProfile');


//        PFR ROUTE
        Route::get('user/lists/pfr', 'UserController@pfrLists');
//        ------------------------------------------------------------

        Route::get('review', 'ReviewController@index');
        Route::get('review/data', 'ReviewController@data');
	    Route::delete('review/{id}', [
	    	'as' => 'review.destroy',
        	'uses' => 'ReviewController@destroy'
    	]);
        Route::post('review/{review}', [
            'as' => 'review.approve',
            'uses' => 'ReviewController@approve'
        ]);

        Route::get('coreg/data', [
            'as' => 'coreg.data',
            'uses' => 'CoregController@data'
        ]);
//        Route::resource('coreg','CoregController');

        Route::get('coreg/create','CoregController@create');
        Route::post('coreg/store','CoregController@store');

        // Add survey form
        Route::get('survey/create','SurveyController@create');
        // Survey List - questions
        Route::get('survey','SurveyController@index');
        // Survey Datatable
        Route::get('survey/data','SurveyController@data');
        // Survey Coreg - cake offers
        Route::get('survey/local','SurveyController@local');
        //  Survey Coreg Datatable - cake offers
        Route::get('survey/local/data','SurveyController@localData');
        //Custom test all lists
        Route::get('survey/designations','SurveyController@designations');

        // Survey lists
        Route::get('survey/offers/list','OffersController@challengeLists');

        // manage questions
        Route::get('question','QuestionController@index');
        // Question list datatable
        Route::get('question/data','QuestionController@data');

        Route::get('question/lists','QuestionController@lists');

        // Question update
        Route::patch('question/update','QuestionController@update');

        // Question deactivate
        Route::patch('question/deactivate','QuestionController@deactivate');

        // Question activate
        Route::patch('question/activate','QuestionController@activate');

        // Question store
        Route::post('question/store','QuestionController@store');

        // Question delete
        Route::delete('question/{id}/delete','QuestionController@destroy');

        // Campaign list
        Route::get('campaign/paginate/list','CampaignController@getPagination');

     

        // Designation update
        Route::patch('designation/update','DesignationController@update');

        // Offer list
        Route::post('offers/store','OffersController@store');
        Route::patch('offers/update','OffersController@update');

        // Routes to latest dashboard----------------------------------------

        Route::get('v2/profile', [
            'as' => 'admin.profile',
            'uses' => 'ProfileController@index'
        ]);

        Route::post('v2/profile/update', [
            'as' => 'admin.profile.update',
            'uses' => 'ProfileController@update'
        ]);

        Route::get('v2/dashboard', [
            'as' => 'admin.dashboard',
            'uses' => 'DashboardController@index'
        ]);

        Route::get('dashboard/statistics', [
            'as' => 'admin.dashboard.statistics',
            'uses' => 'DashboardController@statistics'
        ]);

        Route::get('v2/survey', [
            'as' => 'admin.survey',
            'uses' => 'SurveyController@index'
        ]);

        Route::get('v2/users', [
            'as' => 'admin.users',
            'uses' => 'UserController@index'
        ]);

        Route::get('v2/feedback', [
            'as' => 'admin.feedback',
            'uses' => 'FeedbacksController@index'
        ]);

        Route::get('v2/bad/comments', [
            'as' => 'admin.bad.comments',
            'uses' => 'CommentsController@bad'
        ]);

        Route::get('v2/bad/comments/filter', [
            'as' => 'admin.bad.comments.filter',
            'uses' => 'CommentsController@badfilter'
        ]);

        Route::get('v2/good/comments', [
            'as' => 'admin.good.comments',
            'uses' => 'CommentsController@good'
        ]);

        Route::get('v2/survey/questions', [
            'as' => 'admin.survey.questions',
            'uses' => 'QuestionController@index'
        ]);

        Route::get('v2/zipcodes', [
            'as' => 'admin.zipcodes',
            'uses' => 'ZipcodesController@index'
        ]);

        Route::get('v2/local', [
            'as' => 'admin.local',
            'uses' => 'OffersController@index'
        ]);

        Route::get('offers/lists', [
            'as' => 'admin.offers.lists',
            'uses' => 'OffersController@lists'
        ]);

        Route::get('v2/coregs', [
            'as' => 'admin.coregs',
            'uses' => 'CoregController@index'
        ]);

        Route::get('coregs/lists', [
            'as' => 'admin.coregs.lists',
            'uses' => 'CoregController@lists'
        ]);

        Route::get('coregs/sticker/upload', [
            'as' => 'admin.sticker.upload',
            'uses' => 'CoregController@upload'
        ]);

        Route::post('coregs/update/{id}', [
            'as' => 'admin.coreg.update',
            'uses' => 'CoregController@update'
        ]);

        Route::get('feedbacks/opinion/lists', [
            'as' => 'admin.feedbacks.opinion.lists',
            'uses' => 'FeedbacksController@opinionLists'
        ]);



        Route::get('feedbacks/review/lists', [
            'as' => 'admin.feedbacks.review.lists',
            'uses' => 'ReviewController@lists'
        ]);

        Route::post('resend/leads', [
            'as' => 'admin.resend.leads',
            'uses' => 'UserController@resendLeads'
        ]);

        Route::get('reports', [
            'as' => 'admin.reports',
            'uses' => 'ReportsController@index'
        ]);

        Route::get('reports/data', [
            'as' => 'admin.reports.data',
            'uses' => 'ReportsController@data'
        ]);


        Route::post('user/send/idch', [
            'as' => 'admin.user.send.idch',
            'uses' => 'MailController@sendIdch'
        ]);


        Route::get('user/download/report/{id}', [
            'as' => 'admin.user.download.report',
            'uses' => 'UserController@exportXls'
        ]);

        // Campaign/Coregs delete
        Route::delete('coregs/{id}/delete','CoregController@destroy');

    });


});