<?php

Route::group(['namespace' => 'Frontend'], function () {

    Route::get('test', [
        'as' => 'test.wtt',
        'uses' => 'TestController@test'
    ]);

    Route::get('/', [
        'as' => 'home',
        'uses' => 'HomeController@index'
    ]);

    Route::get('/user-data', [
        'as' => 'user.data',
        'uses' => 'HomeController@userData'
    ]);

    Route::get('challenge', [
        'as' => 'challenge.index',
        'uses' => 'HomeController@challenge'
    ]);

    Route::get('survey-offers', [
        'as' => 'survey.offers',
        'uses' => 'HomeController@offers'
    ]);

    Route::post('hpcs/answers', [
        'as' => 'store',
        'uses' => 'UserAnswerController@store'
    ]);

    Route::post('user/offer', [
        'as' => 'user.offer',
        'uses' => 'UserOfferController@store'
    ]);

    Route::post('contact', [
        'as' => 'contact.send',
        'uses' => 'ContactController@send'
    ]);


    Route::post('facebook/bonus', [
        'as' => 'facebook.bonus.reward',
        'uses' => 'UserOfferController@facebookReward'
    ]);


    Route::get('external/inner/offer', [
        'as' => 'external.iframe.inner',
        'uses' => 'UserOfferController@externalInner'
    ]);

    Route::get('external/outer/offer', [
        'as' => 'external.iframe.inner',
        'uses' => 'UserOfferController@externalOuter'
    ]);

    //CUSTOM RESET PASSWORD
    Route::get('/reset/password', 'MailController@reset');
    Route::get('/reset/password/confirmation/{token}','MailController@index');
    Route::patch('/reset/password/update','MailController@update');
    
    Route::post('/send/feedback', 'MailController@sendFeedback');

});