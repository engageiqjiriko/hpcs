<?php


Route::group(['namespace' => 'Frontend', 'prefix' => 'api/checker'], function () {

    Route::post('zip', [
        'as' => 'checker.zip',
        'uses' => 'CheckerController@zip'
    ]);
    Route::post('email', [
        'as' => 'checker.email',
        'uses' => 'CheckerController@email'
    ]);

});