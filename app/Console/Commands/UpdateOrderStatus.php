<?php

namespace App\Console\Commands;

use App\HPCS\Entities\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateOrderStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offers:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed Database From Cake';

    protected $client;

    protected $bar;

    /**
     * Create a new command instance.z
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->fetchFromCake();

        if ($response->getStatusCode() != 200) {
            $this->error("Can't fetch from Cake.");
            \Log::error('Fetching from cake failed');
            return false;
        }

        $this->info('Done fetching data from cake.');
        $this->info('');

        $data = $this->parseXmlResponse($response);

        $this->info('Updating User Offer Status...');

        $this->bar = $this->output->createProgressBar($data->row_count);

        collect($data->conversions)->filter(function ($conversions) {
            if (is_array($conversions)) {
                return true;
            }

            $this->updateOfferStatus($conversions); //result is just a single row
        })->flatten(1)->map(function ($conversion) {
            $this->updateOfferStatus($conversion);
        });

        $this->bar->finish();
        $this->info('');
        $this->info('All status updated.');

    }


    protected function updateOfferStatus($conversion)
    {

        if ($user = User::find($conversion->sub_id_1)) {
            try {
                $user->offers()->updateExistingPivot($conversion->creative->creative_id, ['approved' => true]);
            } catch (QueryException $e) {}

        }

        $this->bar->advance();
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    protected function fetchFromCake()
    {
        $params = [
            'api_key' => 'zDv2V1bMTwk3my5bUAV3vkue1BJRTQ',
            'start_date' => Carbon::now()->subDays(2)->format('m/d/Y'),
            'end_date' => Carbon::now()->format('m/d/Y'),
            'conversion_type' => 'all',
            'event_id' => 0,
            'source_affiliate_id' => config('hpcs.cake_id'),
            'brand_advertiser_id' => 0,
            'channel_id' => 0,
            'site_offer_id' => 0,
            'site_offer_contract_id' => 0,
            'source_affiliate_tag_id' => 0,
            'brand_advertiser_tag_id' => 0,
            'site_offer_tag_id' => 0,
            'campaign_id' => 0,
            'creative_id' => 0,
            'price_format_id' => 0,
            'disposition_type' => 'all',
            'disposition_id' => 0,
            'source_affiliate_billing_status' => 'all',
            'brand_advertiser_billing_status' => 'all',
            'test_filter' => 'both',
            'start_at_row' => 0,
            'row_limit' => 0,
            'sort_field' => 'conversion_id',
            'sort_descending' => 'false'
        ];

        $this->info('Fetching data from cake...');
        $this->info('This may take a few minutes...');

        return $this->client->request('GET',
            'http://engageiq.performance-stats.com/api/13/reports.asmx/Conversions',
            ['query' => $params]);
    }

    /**
     * @param $response
     * @return mixed
     */
    private function parseXmlResponse($response)
    {
        return json_decode(json_encode(simplexml_load_string($response->getBody()->getContents())));
    }
}
