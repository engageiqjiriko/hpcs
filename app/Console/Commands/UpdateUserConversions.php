<?php

namespace App\Console\Commands;

use App\HPCS\Entities\User;
use App\HPCS\Entities\UserConversion;
use Illuminate\Console\Command;

class UpdateUserConversions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'conversions:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will update users hpcs, sponsors and freebies offers based on cake conversion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->lists = UserConversion::where('executed',0)->get();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lists =  $this->lists;

        foreach ($lists as $list) {

            $user = User::find($list->user_id);
            $hpcs = collect($user->hpcs_taken);
            $offers = collect($user->offers_taken);
            $freebies = collect($user->freebies_taken);

            if($hpcs->where('offer_id',$list->creative_id)->first())
            {
                $newData = $this->preparedString($hpcs,$list->creative_id);
                $user->hpcs_taken = $newData;
                $list->executed = true;
                $list->update();
            }

            if ($offers->where('offer_id',$list->creative_id)->first())
            {
                $newData = $this->preparedString($offers,$list->creative_id);
                $user->offers_taken = $newData;
                $user->update();
                $list->executed = true;
                $list->update();
            }
            elseif ($freebies->where('offer_id',$list->creative_id)->first())
            {
                $newData = $this->preparedString($freebies,$list->creative_id);
                $user->freebies_taken = $newData;
                $user->update();
                $list->executed = true;
                $list->update();
            }
        }
    }

    private function preparedString($collections,$creativeId)
    {
        $temporary = [];
        foreach ($collections as $collection)
        {
            if($collection['offer_id']==$creativeId)
            {
                $collection['status'] = 3;
                array_push($temporary,$collection);
            }
            else
            {
                array_push($temporary,$collection);
            }
        }
        return $temporary;
    }
}
