<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Offer;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;

class OffersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index(){

        return view('admin.local');
    }

    public function lists(Request $request)
    {

        return response(['data' => $this->fetcher($request->q),'message'=>200]);
    }

    private function fetcher($query)
    {
        if($query)
        {
            $offers = Offer::where('name','like','%'.$query.'%')->orderBy('order','ASC')->paginate(15);
        }
        else
        {
            $offers = Offer::orderBy('order','ASC')->paginate(15);
        }

        $offers->getCollection()->map(function ($offers) {
            $offers['date_from_human'] =  $offers->created_at->diffForHumans(Carbon::now());
            $offers['is_type'] =  $offers->is_freebies==0 && $offers->is_sponsors==1 ? 1:0;
            return $offers;
        });

        return $offers;
    }


    public function challengeLists(){

        $offers = Offer::where('general',0)->paginate(15);

        return response(['data' => $offers,'message'=>200]);
    }

    public function store(Request $request)
    {

        $request['our_rating'] = 'Coming Soon..';
        $request['subtitle'] = 'Description';

        if($request->is_type==1)
        {
            $request['is_freebies'] = 0;
            $request['is_sponsors'] = 1;
        }
        else
        {
            $request['is_freebies'] = 1;
            $request['is_sponsors'] = 0;
        }

        $offer = Offer::create($request->all());
        return response(['message'=>200]);

    }

    public function update(Request $request)
    {

        $offer = Offer::find($request->id);

        if($request->is_type==1)
        {
            $offer->is_freebies = 0;
            $offer->is_sponsors = 1;
        }
        else
        {
            $offer->is_freebies = 1;
            $offer->is_sponsors = 0;
        }

        $offer->update($request->all());

        return response(['selected'=>$offer , 'message'=>200]);

    }
}
