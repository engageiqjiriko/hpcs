<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\User;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Contracts\Routing\UrlGenerator;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pheanstalk\Exception;


class MailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function sendIdch(Request $request)
    {
        $request['generated_number'] = $this->generateNumber();


        Mail::send('emails.idch', ['first_name' => $request->first_name, 'generated_number' => $request->generated_number ], function ($message) use ($request)
        {
            $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
            $message->to($request->email);
            $message->subject('High Paying Cash Survey - Lucky User Picked');
        });

        return response(['message' => 'Success', 'code'=>200]);

    }

    private function generateNumber()
    {
        $decimals = 2;
        $min = 70;
        $max = 100;
        $scale = pow(10, $decimals);
        return mt_rand($min * $scale, $max * $scale) / $scale;
    }
}
