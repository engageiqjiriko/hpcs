<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Campaign;
use App\HPCS\Entities\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Yajra\Datatables\Facades\Datatables;


class CoregController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.coreg');
    }

    /**
     * Display a listing for Jquery DataTable.
     *
     * @return json
     */
    public function data()
    {
        return Datatables::of(Campaign::query())->make();
    }




    public function lists(Request $request)
    {
        return response(['data' => $this->fetcher($request->searchedCoreg),'message'=>200]);
    }

    public function fetcher($keyword)
    {
        if($keyword)
        {
            $coregs = Campaign::with('question.designation')->where('name','like', '%' . $keyword . '%')->orderBy('created_at','DESC')->paginate(15);
        }
        else
        {
            $coregs = Campaign::with('question.designation')->orderBy('created_at','DESC')->paginate(15);
        }

        $coregs->getCollection()->map(function ($coregs) {
            $coregs['date_from_human'] =  $coregs->created_at->diffForHumans(Carbon::now());
            return $coregs;
        });

        return $coregs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.coreg.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = \GuzzleHttp\json_decode($request->fields);
        $tempo = [];

        foreach ($fields as $index => $field) {
            foreach ($field as $key => $value) {
                array_push($tempo, [$key => $value]);
            }
        }

        if($request->file)
        {
            $custom_path = 'images/campaigns/';
            $file = $request->file('file');
            $fileName = $custom_path . time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->resize(150,90)->save(public_path($fileName));

            $request['image'] = $fileName;
        }

        if($request->coreg_type=="sponsors")
        {
            $request['freebie'] = 0;
            $request['sponsor'] = 1;
        }
        else
        {
            $campaign['freebie'] = 1;
            $campaign['sponsor'] = 0;
        }

        $request['fields'] = $tempo;
        $request['additional_fields'] = \GuzzleHttp\json_decode($request->additional_fields);

        Campaign::create($request->all());

        return \Response::json(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {

    }


    public function update(Request $request,$id)
    {
        $fields = \GuzzleHttp\json_decode($request->fields);
        $tempo = [];

        foreach ($fields as $index => $field)
        {
            foreach ($field as $key => $value)
            {
                array_push($tempo, [$key=>$value]);
            }
        }
        $campaign = Campaign::find($id);

        if($request->file)
        {
            \Storage::disk('accessible')->delete('/'.$campaign->image);
            $custom_path = 'images/campaigns/';
            $file = $request->file('file');
            $fileName = $custom_path . time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->resize(150,90)->save(public_path($fileName));

            $request['image'] = $fileName;
        }

        if($request->coreg_type=="sponsors")
        {
            $campaign->freebie = 0;
            $campaign->sponsor = 1;
        }
        else
        {
            $campaign->freebie = 1;
            $campaign->sponsor = 0;
        }


        $request['fields'] = $tempo;
        $request['additional_fields'] = \GuzzleHttp\json_decode($request->additional_fields);

        $campaign->update($request->all());

        $question = Question::find($campaign->published_in);
        if($question)
        {
            $question->status = $request->status==1?true:false;
            $question->update();
        }
        return \Response::json(array('success'=>true , 'update' => $campaign));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $campaign->destroy($id);

        return response(['data' => $this->fetcher(null),'message'=>200]);
    }
}
