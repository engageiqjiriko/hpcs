<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Campaign;
use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Offer;
use App\HPCS\Entities\Question;
use App\HPCS\Entities\Survey;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.survey');
    }

    /**
     * Active CLICK OFFERS
     */

//    public function local() //offers
//    {
//        return view('backend.survey.list-local');
//    }

    public function designations()
    {
        $designations = Designation::with('questions.campaign')->with('surveys')->orderBy('order','ASC')->get();

        $designations = $designations->map(function ($designation){
            $designation['q_active'] = $designation->questions->where('status',1)->count();
            $designation['q_inactive'] =  $designation->questions->where('status',0)->count();

            return $designation;

        });

        return response(['data' => $designations,'message'=>200]);
    }


    public function lists(Request $requests){

        if($requests->active==true)
        {
            $data = $offers = Offer::where('general',0)->where('is_active',1)->paginate(500);
        }
        else{
            $data = $offers = Offer::where('general',0)->paginate(500);
        }

        return response(['data' => $data,'message'=>200]);
    }

    /**
     * Display a listing of the resource to data table.
     *
     * DATATABLE DATATABLE DATATABLE DATATABLE DATATABLE
      */

    public function data()
    {

        $questions = Question::with('designation')->get();

        return Datatables::of($questions)
            ->addColumn('action', function ($questions) {
                return '<a href="#" class="btn btn-icon-only default" title="View">
                     <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>' .
                '<a href="#" class="btn btn-icon-only green" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i>
                  </a>'.
                '<a href="#" class="btn btn-icon-only red" title="Remove"><i class="fa fa-trash" aria-hidden="true"></i>
                  </a>';
            })
            ->editColumn('label', function ($questions) {
                return $questions->designation->label;
            })
            ->editColumn('created_at', function ($questions) {
                return $questions->created_at->diffForHumans();
            })
            ->make(true);
    }




    public function localData()
    {
        $offers = Offer::get();

        return Datatables::of($offers)
            ->addColumn('action', function ($questions) {
                return '<a href="#" class="btn btn-icon-only default" title="View">
                     <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>' .
                '<a href="#" class="btn btn-icon-only green" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i>
                  </a>'.
                '<a href="#" class="btn btn-icon-only red" title="Remove"><i class="fa fa-trash" aria-hidden="true"></i>
                  </a>';
            })
            ->editColumn('image', function ($offers) {
                return '<img src="'.$offers->image.'" height="50%" width="100%" />';
            })
            ->editColumn('created_at', function ($offers) {
                return $offers->created_at->diffForHumans();
            })
            ->make(true);
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.survey.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
