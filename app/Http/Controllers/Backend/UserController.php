<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;

use App\HPCS\Entities\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Jobs\SendLeadsToLeadReactor;

use App\HPCSOLD\Entities\User as UserOld;
use App\HPCSOLD\Entities\UserProfile as UserProfile;

use App\HPCSPFR\Entities\User as UserPfr;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['store']]);
    }

    public function index()
    {
        return view('admin.user');
    }

    public function lists(Request $request)
    {
        return response(['users' => $this->fetcher($request->all()),'code'=>200 ], Response::HTTP_OK);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->city = $request->city;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->mail_address = $request->mail_address;
        $user->update();

        return response(['users' => $this->fetcher($request->all()), 'selectedUser' => $user ,'code'=>200 ], Response::HTTP_OK);
    }

    public function fetcher($request)
    {
        if($request['paymentsBy']!='all' && $request['searchedUser'])
        {

            switch ($request['paymentsBy'])
            {
                case 'hpcs' :
                    $user = User::with('cake.offerName')
                    ->where('payment_status', 'not like', '%"for":"hpcs"%')
                    ->orWhere('payment_status',null)
                    ->where('registered',1)
                    ->where('hpcs_taken','<>',null)
                    ->orderBy($request['sortBy'],$request['orderBy'])
                    ->paginate($request['resultLimit']);
                    break;

                case 'challenge' :
                    $user = User::with('cake.offerName')
                    ->where('payment_status', 'not like', '%"for":"challenge"%')
                    ->orWhere('payment_status',null)
                    ->where('created_at','<',Carbon::now()->subDay(30))
                    ->where('registered',1)
                    ->where('challenge_taken','<>',null)
                    ->orderBy($request['sortBy'],$request['orderBy'])
                    ->paginate($request['resultLimit']);
                    break;

                case 'linkout' :
                    $user = User::with('cake.offerName')
                        ->where('registered',1)
                        ->where('hpcs_taken','like','%"status":3%')
                        ->orderBy($request['sortBy'],$request['orderBy'])
                        ->paginate($request['resultLimit']);
                    break;
            }
        }
        else
        {
            $user = User::with('cake.offerName')
                ->where('email','like', '%' . $request['searchedUser'] . '%') //search use email
                ->where('registered', 1)
                ->orWhere('id', $request['searchedUser']) //search use email
                ->orderBy($request['sortBy'],$request['orderBy'])
                ->paginate($request['resultLimit']);
        }


        $user->getCollection()->map(function ($user)
        {
            $user = $this->userAttachment($user); // generate additional fields
            $user['converted_leads'] = collect($user->coregs)->filter(function ($coregs) {
                if(isset(json_decode($coregs->pivot->nlr_response)->realtime_meaning) && json_decode($coregs->pivot->nlr_response)->realtime_meaning=='Success')
                {
                    return $coregs;
                }
                return false;
            })->count();

            return $user;
        });

        return $user;
    }
//---------------------------------------------------------------------------------------
    public function oldLists(Request $request)
    {
        if($request->searchedUser)
        {
            $user = UserOld::where('is_admin', '=', 0)->where('email','like', '%' . $request->searchedUser . '%')->orderBy('created_at', 'DESC')->paginate(20);
        }
        else
        {
            $user = UserOld::where('is_admin', '=', 0)->orderBy('created_at', 'DESC')->paginate(20);
        }

        $user->getCollection()->map(function ($user) {
            $user['date_from_human'] =  $user->created_at->diffForHumans(Carbon::now());
            $user['money_hpcs'] = $user->points;
            return $user;
        });

        return response(['users' => $user,'code'=>200], Response::HTTP_OK);
    }

    public function oldProfile($id)
    {

        $user = UserProfile::where('user_id',$id);

        return $user->first();
    }
// ---------------------------------------------------------------------------------------


    public function pfrLists(Request $request)
    {
        if($request->searchedUser)
        {
            $user = UserPfr::where('EMAIL','like', '%' . $request->searchedUser . '%')->orderBy('CREATE_DATE', 'DESC')->paginate(20);
        }
        else
        {
            $user = UserPfr::orderBy('CREATE_DATE', 'DESC')->paginate(20);
        }

        $user->getCollection()->map(function ($user) {
            $user['date_from_human'] =  Carbon::createFromFormat('Y-m-d H:i:s', $user->CREATE_DATE)->diffForHumans(Carbon::now());
            $user['money_hpcs'] = 'N/A';
            return $user;
        });

        return response(['users' => $user,'code'=>200], Response::HTTP_OK);
    }

    // ---------------------------------------------------------------------------------------

    public function data()
    {
    	$user = User::where('registered', 1)->get();
        
    	return Datatables::of($user)
    	->addColumn('action', function ($user) {
                return '<a href="#" class="btn btn-icon-only default" title="View">
                     <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>' .
                '<a href="#" class="btn btn-icon-only green" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i>
                  </a>'.
                  '<a href="#" class="btn btn-icon-only red" title="Remove"><i class="fa fa-trash" aria-hidden="true"></i>
                  </a>';
            })
    	->editColumn('created_at', function ($user) {
            return $user->created_at->diffForHumans();
        })
        ->addColumn('money', function ($user) {
            $hpcs = collect($user->hpcs_taken)->where('status',4)->sum('cash');
            $challenge = collect($user->challenge_taken)->where('status',4)->sum('cash');
            return $hpcs+$challenge;
        })
        ->addColumn('payment', function ($user) {
            return 0;
        })
        ->addColumn('request-date', function ($user) {
            return 'no record';
        })
		->make(true);
    }

    public function resendLeads(Request $request)
    {
        $user = User::find($request->id);

        $this->dispatch((new SendLeadsToLeadReactor($user))->delay(1));
    }






    public function sendPayment(Request $request)
    {
        $user = $this->paymentHandler($request);



        

        if($user)
        {
            return response(['users' => $this->fetcher($request->all()), 'selected' => $this->userAttachment($user),'code'=>200], Response::HTTP_OK);
        }
        return response(['code'=>500], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    private function paymentHandler($request)
    {
        $user = User::find($request['id']);

        switch($request['type'])
        {
            case 'hpcs' :
                $payment = collect($user->hpcs_taken);
                $cash = $payment->where('status',4)->where('is_Offer',0)->sum('cash');
                $payment->transform(function ($item, $key) {
                    if($item['status'] == 4 && $item['is_Offer']==0)
                    {
                        $item['status'] = 5;
                    }
                    return $item;
                });

               $data = [
                    'for' => 'hpcs',
                    'cash' => $cash,
                    'method' =>$request['payment_method'],
                    'date' => Carbon::now(),
                    'note'=> $request['note']
               ];

                $tempo= [];
                if($user->payment_status)
                {
                    $tempo = $user->payment_status;
                }


                array_push($tempo,$data);
                $user->hpcs_taken = $payment;
                $user->payment_status = $tempo;
                $user->update();

                $this->paymentMail($user->email,$user->first_name,$cash);
                return $user;

            break;

            case 'challenge' :
                $payment = collect($user->challenge_taken);
                $cash = $payment->where('status',4)->sum('cash');
                $payment->transform(function ($item, $key) {
                    if($item['status'] == 4)
                    {
                        $item['status'] = 5;
                    }

                    return $item;
                });

                $data = [
                    'for' => 'challenge',
                    'cash' => $cash,
                    'method' =>$request['payment_method'],
                    'date' => Carbon::now(),
                    'note'=> $request['note']
                ];

                $tempo= [];
                if($user->payment_status)
                {
                    $tempo = $user->payment_status;
                }
                array_push($tempo,$data);
                $user->challenge_taken = $payment;
                $user->payment_status = $tempo;
                $user->update();

                $this->paymentMail($user->email,$user->first_name,$cash);

                return $user;
            break;

            case 'linkout' :
                $payment = collect($user->hpcs_taken);
                $cash = $payment->where('status',3)->where('is_Offer',1)->sum('cash');
                $payment->transform(function ($item, $key) {
                    if($item['status'] == 3 && $item['is_Offer']==1)
                    {
                        $item['status'] = 5;
                    }

                    return $item;
                });

                $data = [
                    'for' => 'linkout',
                    'cash' => $cash,
                    'method' =>$request['payment_method'],
                    'date' => Carbon::now(),
                    'note'=> $request['note']
                ];

                $tempo= [];
                if($user->payment_status)
                {
                    $tempo = $user->payment_status;
                }
                array_push($tempo,$data);
                $user->hpcs_taken = $payment;
                $user->payment_status = $tempo;
                $user->update();

                $this->paymentMail($user->email,$user->first_name,$cash);

                return $user;
            break;

            default :
                return false;
        }
    }

    private function paymentMail($email,$first_name,$value)
    {
        \Log::notice('Payment Mail TRIGGER');

//        Mail::send('emails.payment', ['first_name' => $first_name, 'value' => $value ], function ($message) use ($email)
//        {
//            $message->from('no-reply@highpayingcashsurveys.com', 'High Paying Cash Surveys');
//            $message->to($email);
//            $message->subject('High Paying Cash Survey - Send Payment');
//        });

    }


    private function userAttachment($user)
    {
        $hpcs_taken = collect($user->hpcs_taken);
        $challenge_taken = collect($user->challenge_taken);
        $offers_taken = collect($user->offers_taken);
        $freebies_taken = collect($user->freebies_taken);
        $user_coregs = collect($user->coregs);

        $user['date_from_human'] =  $user->created_at->diffForHumans(Carbon::now());
        $user['money_hpcs'] = $hpcs_taken->where('is_Offer',0)->where('status',4)->sum('cash');
        $user['money_hpcs_linkout'] = $hpcs_taken->where('is_Offer',1)->where('status',3)->sum('cash');
        $user['money_hpcs_paid'] = $hpcs_taken->where('is_Offer',0)->where('status',5)->sum('cash');
        $user['money_hpcs_linkout_paid'] = $hpcs_taken->where('is_Offer',1)->where('status',5)->sum('cash');

        $user['money_challenge'] = $challenge_taken->where('status',4)->sum('cash');
        $user['money_challenge_paid'] = $challenge_taken->where('status',5)->sum('cash');

        $user['money_offers'] = $user_coregs->where('pivot.is_offers',1)->sum('pivot.cash') + $offers_taken->whereIn('status',[1,3,4,5])->sum('cash');
        $user['count_freebies'] = $user_coregs->where('pivot.is_offers','<>',1)->where('pivot.status',4)->count('pivot') + $freebies_taken->whereIn('status',[1,3,4,5])->count('status');


        $user['unsend_leads'] = $user_coregs->where('pivot.status',1)->count('pivot');
        $user['success_leads'] = $user_coregs->where('pivot.status',3)->count('pivot');
        $user['reject_leads'] = $user_coregs->where('pivot.status',2)->count('pivot');

        $user['challenge_payment_active'] = $user->created_at->diffInDays(Carbon::now()) < 31;

        return $user;

    }


    public function exportXls($id)
    {
        $user = User::find($id);
        $data = [];

        foreach ($user->coregs as $coreg)
        {
            if($coreg->pivot->status>1)
            {
                if($coreg->pivot->nlr_response)
                {
                    $result = collect(\GuzzleHttp\json_decode($coreg->pivot->nlr_response));
                }

                array_push($data,[
                    'Campaign Name' => $coreg->campaign->name,
                    'NLR visibility' =>$result['message']=='Lead received for processing' ? 'Yes' : 'No',
                    'Status Code' => $coreg->pivot->status,
                    'Response' => $result['message']=='Lead received for processing' ? $result['realtime_meaning'] : $result['message'],
                    'Message' => $result['message']=='Lead received for processing' ? $result['realtime_message'] : '',
                ]);
            }
            else
            {
                array_push($data,[
                    'Campaign name' => $coreg->campaign->name,
                    'NLR visibility' => 'No',
                    'Status Code' => $coreg->pivot->status,
                    'Response' => 'Pending / Submitting',
                    'Message' => 'N/A'
                ]);
            }
        };

        $data = collect($data)->sortBy('Campaign name')->toArray();

        Excel::create($user->email.' '.$user->last_name.'- Coreg Report', function($excel) use ($data) {
            // Set the title
            $excel->setTitle('Our new awesome title');
            // Chain the setters
            $excel->setCreator('HPCS-REG')
                ->setCompany('EIQ');
            // Call them separately
            $excel->setDescription('User Campaign Report');

            $excel->sheet('Sheetname', function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        })->export('xlsx');
    }

}
