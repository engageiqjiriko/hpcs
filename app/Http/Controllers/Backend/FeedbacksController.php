<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Opinions;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class FeedbacksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.feedback');
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function opinionLists()
    {
        $opinions = Opinions::orderBy('created_at','DESC')->paginate(15);

        $opinions->getCollection()->map(function ($opinions) {

            if($this->isJson($opinions->opinion))
            {
                $break = collect(\GuzzleHttp\json_decode($opinions->opinion));
                $opinions['ip'] =  $break['ip'];
                $opinions['name'] =  $break['name'];
                $opinions['text'] =  $break['text'];
            }
            else
            {
                $opinions['ip'] =  '0.0.0.0';
                $opinions['name'] =  'Anonymous';
                $opinions['text'] =  $opinions->opinion;
            }

            $opinions['date_from_human'] =  $opinions->created_at->diffForHumans(Carbon::now());
            return $opinions;
        });

        return response(['data' => $opinions,'message'=>200]);
    }
}
