<?php

namespace App\Http\Controllers\Backend;

use App\HPCS\Entities\Campaign;
use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Question;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Pheanstalk\Exception;
use Yajra\Datatables\Datatables;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.survey-question');
//        return view('backend.question.list');
    }

//    public function data()
//    {
//        $questions = Question::with('designation')
//            ->where('campaign_id',null)->get();
////            ->paginate(15);
//
//
//        return response(['data' => $questions, 'message'=>200]);
//    }

    public function lists()
    {
        $questions = Question::with('designation')->orderBy('designation_id','ASC')->paginate(20);

        $questions->getCollection()->map(function ($questions) {
            $questions['date_from_human'] =  $questions->created_at->diffForHumans(Carbon::now());
            return $questions;
        });

        return response(['data' => $questions,'message'=>200]);
    }


    public function data()
    {
        $questions = Question::with('designation')
            ->where('campaign_id',null)
            ->get();

        return Datatables::of($questions)
            ->addColumn('action', function ($questions) {

                $messenger = '
                     data-id="'.$questions->id.'"
                     data-mimic="'.encrypt($questions->id).'"
                     data-designation="'.$questions->designation_id.'"
                     data-label="'.$questions->designation->label.'"
                     data-title="'.$questions->title.'"
                     data-content="'.$questions->content.'"
                     data-order="'.$questions->order.'"
                     data-lead_reactor="'.$questions->lead_reactor.'"
                     data-polar="'.$questions->polar.'"
                     data-created="'.$questions->created_at.'"
                     data-updated="'.$questions->updated_at.'"';

                return '<button class="btn btn-icon-only view-question" title="View" '. $messenger . '>
                              <i class="fa fa-eye" aria-hidden="true"></i>
                        </button>' .
                        '<button class="btn btn-icon-only green edit-question" title="Edit" '. $messenger . '>
                              <i class="fa fa-pencil" aria-hidden="true"></i>
                         </button>'.
                        '<button class="btn btn-icon-only red delete-question" title="Remove" '. $messenger . '>
                              <i class="fa fa-trash" aria-hidden="true"></i>
                         </button>';
            })
            ->editColumn('label', function ($questions) {
                return $questions->designation->label;
            })
            ->editColumn('created_at', function ($questions) {
                return $questions->created_at->diffForHumans();
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $max = Question::where('designation_id',$request->designation_id)->get();
        $request['order'] = collect($max)->max('order')+1;
        $request['status'] = 1;

        if(!$request->offer_id)
        {
            $request['offer_id'] = 0;
        }
        $question = Question::create($request->all());

        $campaign = Campaign::find($request->campaign_id);

        if($campaign)
        {
            $campaign->published_in =$question->id;
            $campaign->update();
        }

        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $question = Question::find($request->id);
            $question->title = $request->title;
            $question->content = $request->contents;
            $question->update();


            $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
            return response(['data' => $designations, 'message'=>200]);
    }


    public function activate(Request $request)
    {
        $question = Question::find($request->id);
        $question->status = true;
        $question->update();


        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);
    }

    public function deactivate(Request $request)
    {

        $question = Question::find($request->id);
        $question->status = false;
        $question->update();


        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->destroy($id);

        if($question->campaign_id)
        {
            $campaign = Campaign::find($question->campaign_id);
            $campaign->published_in = 0;
            $campaign->update();
        }


        $designations = Designation::with('questions')->with('surveys')->orderBy('order','ASC')->get();
        return response(['data' => $designations, 'message'=>200]);

    }
}
