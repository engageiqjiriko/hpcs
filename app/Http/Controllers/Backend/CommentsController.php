<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

    }

    public function good()
    {
        return view('admin.good-comments');
    }

    public function bad()
    {
        return view('admin.bad-comments');
    }
    public function badfilter()
    {
        return view('admin.bad-comments-filter');
    }

}
