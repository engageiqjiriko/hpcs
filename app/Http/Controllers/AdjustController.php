<?php

namespace App\Http\Controllers;

use App\HPCS\Entities\Designation;
use App\HPCS\Repositories\Survey;
use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\Response;


class AdjustController extends Controller
{
    protected $survey;

    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }

    public function adjust(Request $request)
    {

            $user = \Guest::user();
            $level = $request->level;

            \Log::notice('Navigation Page' . $user->id . '-' . $level);

            switch($level){

                case 1 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 1;
                    $user->current_page_level = 1;
                    break;
                }

                case 2 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 2;
                    $user->current_page_level = 2;
                    break;
                }

                case 3 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 3;
                    $user->current_page_level = 3;
                    break;
                }

                case 4 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 4;
                    $user->current_page_level = 4;
                    break;
                }

                case 5 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 5;
                    $user->current_page_level = 5;
                    break;
                }

                case 6 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 6;
                    $user->current_page_level = 6;
                    break;
                }

                case 7 :
                {
                    $user->can_challenge = 0;
                    $user->hpcs_designation = 7;
                    $user->current_page_level = 7;
                    break;
                }

                case 8 :
                {
                    $user->can_challenge = 1;
                    $user->hpcs_designation = 8;
                    $user->current_page_level = 8;
                    break;
                }

                case 9 :
                {
                    $user->can_challenge = 1;
                    $user->hpcs_designation = 8;
                    $user->current_page_level = 9;
                    break;
                }

                case 10 :
                {
                    $user->can_challenge = 1;
                    $user->hpcs_designation = 8;
                    $user->current_page_level = 9;
                    break;
                }

                default :
                {
                    $user->can_challenge = 1;
                    $user->hpcs_designation = 8;
                    $user->current_page_level = 9;
                    break;
                }
            }

            $this->desginationChecker(collect($user->designation_done),$user,$request->operation);

            $user->update();
            $questions = null;

            $survey = new Survey();

            if($user->can_challenge==1)
            {
                $hpcs = $this->survey->fetchChallengeDesignation($user->challenge_designation);
                $questions = $this->survey->fetchChallengeQuestions($hpcs);

                return response(
                    [
                        'user' => $survey->money($user),
                        'hpcs' => $hpcs ,
                        'questions' => $questions,
                    ],
                    Response::HTTP_CREATED
                );
            }
            else
            {
                $hpcs = $this->survey->fetchHPCSDesignation($user);
                $questions = $this->survey->fetchQuestions($user,$level);

                return response(
                    [
                        'user' => $survey->money($user),
                        'questions' => $questions,
                        'hpcs' => $hpcs,
                    ],
                    Response::HTTP_CREATED
                );
            }
    }

    private function desginationChecker($designations,$user,$operation)
    {

        if ($designations->contains($user->hpcs_designation)) {
            if ($operation == 'forward') {
                $user->hpcs_designation++;
                $user->current_page_level++;

                if ($user->hpcs_designation > 7) {
                    $user->can_challenge = 1;
                } else {
                    $user->can_challenge = 0;
                }

                $this->desginationChecker($designations, $user, $operation);

            } else if ($operation == 'backward') {
                $user->hpcs_designation--;
                $user->current_page_level--;


                if ($user->hpcs_designation > 7) {
                    $user->can_challenge = 1;
                } else {
                    $user->can_challenge = 0;
                }

                $this->desginationChecker($designations, $user, $operation);
            }

        } else {
            return true;
        }
    }

}
