<?php

namespace App\Http\Controllers\Frontend\Traits;

trait ValidateAnswers
{
    protected function userHasAnsweredCorrectly($answers, $designation)
    {
        return $designation->no_of_questions >=  $answers->count();

        //$validator = $this->validateAnswers($designation->questions, $answers->toArray());
    }

    protected function validateAnswers($questions, $answers)
    {
        $answers = $this->combineForValidation($questions, $answers);

        $rules = $this->createRules($questions);

        return \Validator::make($answers, [$rules])->passes();
    }

    protected function combineForValidation($questions, $anwers)
    {
        $keys = $questions->map(function ($val, $index) {
            return 'question_' . $index;
        })->toArray();

        return array_combine($keys, $anwers);
    }

    protected function createRules($questions)
    {
        return $questions->map(function ($val, $index) {
            return ['question_' . $index => 'required'];
        })->toArray();
    }

    /* protected function createRules($questions)
     {
         return $questions->map(function ($val, $index) {
             $validators = collect($val->validators);
             return ['question_' . $index => $validators->reduce(function ($prev, $next) use ($validators) {
                 return $prev .= $validators->last() == $next ? $next : $next . '|';
             }, '')];
         })->toArray();
     }*/
}