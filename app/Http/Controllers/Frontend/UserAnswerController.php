<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\Freebies;
use App\HPCS\Repositories\Survey;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\Traits\ValidateAnswers;

use App\Http\Requests\UserAnswerRequest;
use App\Jobs\SaveUserAnswers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;

class UserAnswerController extends Controller
{
    use ValidateAnswers;

    protected $survey;
    protected $user;

    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }

    public function store(Request $request)
    {
        
        $this->user = \Guest::user();

        $filteredAdditional = collect();

        if (!$this->userHasAnsweredCorrectly
        (
            $answers = collect(request()->answers),
            $designation = $this->survey->getUserDesignation($this->user),
            $additional =  collect(request()->additional),
            $questions_length = $request->qlength
        )
        )
        {
            return response(['status' => 'failed', 'message' => 'fields not correct'], Response::HTTP_BAD_REQUEST);
        }

        if (!$this->user) {
            $this->user = \Guest::addCookieForUser();
            $this->user->pp_tos = true;
        }

        if($additional)
        {
            $collection = $answers->each(function ($item, $key) use ($additional, $filteredAdditional, $request) {
                if($item>0)
                {
                    $values = $additional->get($key);
                    $tempo = [
                        ['xxTrustedFormToken' => $request->xxTrustedFormToken],
                        ['xxTrustedFormCertUrl' => $request->xxTrustedFormCertUrl]
                    ];

                    if($values)
                    {
                        foreach ($values as $field => $value)
                        {
                            if(!$value || $value=='')
                            {
                                $value = null;
                            }

                            array_push($tempo,[$field=>$value]);
                        }
                    }
                    $filteredAdditional->put($key,$tempo);
                }
            });
        }


        $this->dispatch(new SaveUserAnswers($answers, $designation, $this->user, $filteredAdditional,$questions_length));

        $this->designationChecker();
        $questions = $this->survey->fetchQuestions($this->user,1);
        $hpcs = $this->survey->fetchHPCSDesignation($this->user);

        $this->user->update();

        return response(
            [
                'user' => $this->survey->money($this->user->max_page_level==2 ? $this->user : \Guest::user()),
                'questions'=> $questions,
                'hpcs' => $hpcs,
            ],
            Response::HTTP_CREATED
        );

    }


    private function designationChecker()
    {
        $designations = collect($this->user->designation_done);

        if($designations->contains($this->user->hpcs_designation))
        {
            $this->user->hpcs_designation++;
            $this->user->current_page_level++;

            if($this->user->current_page_level>7)
            {
                $this->user->can_challenge = 1;
            }
            else
            {
                $this->user->can_challenge = 0;
            }

            $this->designationChecker();

        }
    }


}
