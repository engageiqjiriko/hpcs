<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Freebies;
use App\HPCS\Entities\Question;
use App\HPCS\Entities\User;
use App\HPCS\Repositories\Survey;
use App\HPCS\Session\Guest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use Pheanstalk\Exception;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    protected $survey;

    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }

    /**
     * Home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

  
    public function index()
    {
//
        if(config('hpcs.site_mode')=='MAINTENANCE')
        {
            abort(401);
        }

        if(!Auth::user())
        {
            if(isset($_SERVER['HTTP_REFERER']))
            {
                session(['user_origin' => $_SERVER['HTTP_REFERER']]);
            }
            else
            {
                session(['user_origin' => 'Unknown']);
            }
        }
        
        $options = [
            'link' => null,
            'env' => config('app.env'),
            'setup' => 'normal'
        ];

        return view('frontend.home.index')
            ->withOptions(\GuzzleHttp\json_encode($options))
            ->withPrepop(json_encode($this->prepop()))->withDevice(json_encode($this->agent()));
    }
    /**
     * Api for user data
     * Route: user.data /user-data
     *
     * @return Json
     */
    public function userData()
    {
        $user = \Guest::user();
        $freebies = null;

        if($user)
        {
            if($user->can_challenge < 2 )
            {

                if(!$user->first_challenge)
                {
                    $diffInDays = 0;
                }
                else
                {
                    $diffInDays = $user->first_challenge->diffInDays(Carbon::today());
                }


                if($user->challenge_designation<$diffInDays)
                {
                    $user->challenge_designation = $diffInDays;
                }

                if($diffInDays>30)
                {
                    $user->can_challenge = 2;
                }
                $user->update();
            }
        }
        return response(
            [
                'user' =>  $this->survey->money($user),
                'questions' => $this->survey->fetchQuestions($user,1),
                'hpcs' => $this->survey->fetchHPCSDesignation($user),
            ], Response::HTTP_ACCEPTED
        );
    }

    /**
     * Api to get the Offers
     * Route: offers.index /offers
     *
     * @return mixed
     */

    public function offers()
    {
        $user = session()->get('prepop') ? session()->get('prepop') : \Guest::user()->toArray();

        if(!isset($user['id']))
        {
            $user['id'] = \Guest::user()->id;
        }

        $offers = $this->survey->fetchOffers();

        return $this->survey->offer_params($offers,$user);
    }
    /**
     * Api to get the 30 day challenge questions
     * Route: challenge.index /challenge
     *
     * @return Response
     */
    public function challenge()
    {
        if (!$this->userCanChallengeToday($user = \Guest::user())) {
            return response(['status' => 'failed', 'message' => 'Already done challenge or not yet allowed.'],
                Response::HTTP_BAD_REQUEST);
        }


        $user->current_page_level=8;

        if(!$user->first_challenge)
        {
            $user = User::find($user->id);
            $user->first_challenge = Carbon::now();
        }

        $user->update();

        $designation = $this->survey->fetchChallengeDesignation($user->challenge_designation);
        $challenge = $this->survey->fetchChallengeQuestions($designation);

        $user_info = session()->get('prepop') ? session()->get('prepop') : $user;

        if($user->can_challenge==1)
        {
            if(!isset($user_info['id']))
            {
                $user_info['id'] = $user->id;
            }

            $challenge = $challenge->map(function($question) use ($user_info){

                if($question->offer) {
                    $parameters = [];

                    $parameters['a'] = config('hpcs.cake_id');
                    $parameters['c'] = $question->offer->id;
                    $parameters['s1'] = '';
                    $parameters['s4'] = $user_info['id'];

                    if($question->offer->click_paid)
                    {
                        $parameters['p'] = 'c';
                    }

                    $parameters['email'] = isset($user_info['email']) ? $user_info['email'] : null;
                    $parameters['firstname'] = isset($user_info['first_name']) ? $user_info['first_name'] : null;
                    $parameters['lastname'] =  isset($user_info['last_name']) ? $user_info['last_name'] : null;
                    $parameters['first_name'] = isset($user_info['first_name']) ? $user_info['first_name'] : null;
                    $parameters['last_name'] =  isset($user_info['last_name']) ? $user_info['last_name'] : null;
                    $parameters['fname'] =  isset($user_info['first_name']) ? $user_info['first_name'] : null;
                    $parameters['lname'] =  isset($user_info['last_name']) ? $user_info['last_name'] : null;
                    $parameters['dobmonth'] = isset($user_info['dob']) ? Carbon::parse($user_info['dob'])->format('m') : null;
                    $parameters['dobday'] = isset($user_info['dob']) ? Carbon::parse($user_info['dob'])->format('d') : null ;
                    $parameters['dobyear'] =   isset($user_info['dob']) ? Carbon::parse($user_info['dob'])->format('y') : null;
                    $parameters['zip'] = isset($user_info['zip']) ? $user_info['zip'] : null;
                    $parameters['gender'] =  isset($user_info['gender']) ? $user_info['gender'] : null;
                    $parameters['address'] =  isset($user_info['mail_address']) ? $user_info['mail_address'] : null;
                    $parameters['state'] = isset($user_info['state']) ? $user_info['state'] : null;
                    $parameters['phone1'] =  isset($user_info['phone1']) ? $user_info['phone1'] : null;
                    $parameters['phone2'] =   isset($user_info['phone2']) ? $user_info['phone2'] : null;
                    $parameters['phone3'] =  isset($user_info['phone3']) ?$user_info['phone3'] : null ;

                    $question->offer['cake_link'] = 'https://engageiq.nlrtrk.com/?' . http_build_query($parameters);
                }
            return $question;
            });
        }

        return response(
            [
                'user' => $this->survey->money($user),
                'hpcs' => $designation,
                'questions' => $challenge,
            ],
            Response::HTTP_OK
        );
    }

    private function userCanChallengeToday($user)
    {

         if($user->first_challenge == null)
         {
             return true;
         }

        if(Carbon::now()->diffInDays($user->first_challenge) < 31)
        {
            if(!$user->challenge_taken)
            {
                return true;
            }
            if(Carbon::parse(collect($user->challenge_taken)->last()['taken']) < Carbon::today())
            {
                return true;
            }
         }

        return false;

//        if($user->last_challenge==null)
//        {
//            return true;
//        }
//        return $user->last_challenge == null
//        || $user->first_challenge == null
//        || $user->last_challenge->diffInDays(Carbon::today())>0;
////        && $user->can_challenge;
    }

    public function agent()
    {
        $device = [];

        $agent = new Agent();
        $device['type'] = $agent->isTablet() ? 'Tablet' : $agent->isMobile() ? 'Mobile' : $agent->isDesktop() ? 'Desktop' :  'Unknown';
        $device['os'] = $agent->platform().' version '.$agent->version($agent->platform());
        $device['agent'] = $_SERVER['HTTP_USER_AGENT'];
        $device['browser'] = $agent->browser().' '.$agent->version($agent->browser());

        return $device;
    }

    public function prepop()
    {
        $prepop = [];
        if($user = \Guest::user())
        {
            if(!$user->registered && request()->all())
            {
                $user['first_name'] = request()->firstname;
                $user['last_name'] = request()->lastname;
                $user['zip'] = request()->zip;
                $user['ip'] = request()->ip();
                $user['city'] = request()->city;
                $user['state'] = request()->state;
                $user['email'] = request()->email;
                $user['gender'] = request()->gender;
                $user['mail_address'] = request()->address;
                $user['phone1'] = request()->phone1;
                $user['phone2'] = request()->phone2;
                $user['phone3'] = request()->phone3;
                $user['dob'] = Carbon::createFromDate(request()->dobyear,request()->dobmonth,request()->dobday)->format('Y-m-d');

                $prepop = collect($user)->forget([
                    'coregs',
                    'designation_done',
                    'questions_answered',
                    'hpcs_taken',
                    'challenge_taken',
                    'offers_taken',
                    'freebies_taken',
                    'created_at',
                    'updated_at',
                    'first_challenge',
                    'last_challenge',
                    'offer_progress'
                ])->toArray();

                session(['prepop' => $prepop]);
            }
            else
            {
                session(['prepop' => null]);
            }
        }
        else
        {
            if(request()->all())
            {
                $prepop['first_name'] = request()->firstname;
                $prepop['last_name'] = request()->lastname;
                $prepop['zip'] = request()->zip;
                $prepop['ip'] = request()->ip();
                $prepop['city'] = request()->city;
                $prepop['state'] = request()->state;
                $prepop['email'] = request()->email;
                $prepop['gender'] = request()->gender;
                $prepop['mail_address'] = request()->address;
                $prepop['dob'] = Carbon::createFromDate(request()->dobyear,request()->dobmonth,request()->dobday)->format('Y-m-d');
                $prepop['phone1'] = request()->phone1;
                $prepop['phone2'] = request()->phone2;
                $prepop['phone3'] = request()->phone3;
                session(['prepop' => $prepop]);
            }
            else
            {
                session(['prepop' => null]);
            }
        }

        return $prepop;
    }

}
