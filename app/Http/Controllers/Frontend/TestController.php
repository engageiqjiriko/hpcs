<?php

namespace App\Http\Controllers\Frontend;

use App\HPCS\Entities\Admin;
use App\HPCS\Entities\Conversion;
use App\HPCS\Entities\Test;
use App\Http\Controllers\Controller;
use App\Notifications\UserMadeContact;

class TestController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = \Guest::user(); //money = 5
    }

    public function test()
    {
        dd(\Guest::user());
        $admin = Admin::first();
        $admin->notify(new UserMadeContact);

    }

}
