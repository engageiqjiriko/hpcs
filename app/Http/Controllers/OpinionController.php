<?php

namespace App\Http\Controllers;

use App\HPCS\Entities\Opinions;
use App\HPCS\Entities\User;
use Illuminate\Contracts\Routing\UrlGenerator;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Pheanstalk\Exception;


class OpinionController  extends Controller
{

    public function __construct()
    {

    }

    public function store(Request $request){

        $data = [
            "ip" => $request->ip(),
            "name" => $request->sender,
            "text" => $request->opinion
        ];

        $request['opinion'] = \GuzzleHttp\json_encode($data);

        try{
            Opinions::create($request->all());
            return response(['message' => 'Success', 'code'=>200]);
        }
        catch (Exception $e)
        {
            return response(['message' => 'Error', 'code'=>500]);
        }

    }



}
