<?php

namespace App\HPCSOLD\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'mysql_2';

    public function profile()
    {
        $this->hasOne(UserProfile::class);
    }


}
