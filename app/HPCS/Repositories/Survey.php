<?php

namespace App\HPCS\Repositories;

use App\HPCS\Entities\Category;
use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Offer;
use App\HPCS\Entities\Question;
use App\HPCS\Entities\Setting;
use Carbon\Carbon;

class Survey
{
    /**
     * Returns the users designation
     *
     * @return Designation $designation
     */
    public function getUserDesignation($user)
    {
        //check if user is already on 30 day challenge

        if ($user && $user->can_challenge) {
            return $this->fetchChallengeDesignation($user->challenge_designation);
        }

        if (!$user) {
            return $this->getDesignation(1);
        }

        return $this->getDesignation($user->hpcs_designation);
    }


    public function fetchHPCSDesignation($user)
    {
        if (!$user) {
            return $this->getDesignation(1);
        }

        if ($user->hpcs_designation > Setting::maxHpcs()) {
            return null;
        }

        return $this->getDesignation($user->hpcs_designation);
    }

    public function fetchQuestions($user,$operation=null)
    {
        if (!$user) {
            //get the first designation
            return \Cache::remember('hpcs_designation1', 1, function () {
                return Designation::with('questions')->with('questions.campaign')->where('order',1)->first()->questions;
            });
        }

        if ($user->exceedDummyQuestions()) {
            return null;
        }

        $designation = $user->hpcs_designation;
        $user_answers = $user->questions_answered;

        $answered_ady = [];
        if($user_answers)
        {
            foreach ($user_answers as $answer)
            {
                if($answer['v']>0)
                {
                    array_push($answered_ady,$answer['i']);
                }
            }
        }

        //get the user's current designation
        if($operation!=null && $user->current_page_level>2)
        {
            return Designation::with(['questions' => function($q) use ($answered_ady) {
                $q->where('status',1)->whereNotIn('id',$answered_ady )->with('campaign');
            }])->where('order',$designation)->first()->questions;
        }

        return \Cache::remember('hpcs_designation' . $designation, 1, function () use ($designation, $answered_ady) {
            return Designation::with(['questions' => function($q)
            {
                $q->where('status',1)->with('campaign');
            }
            ])->where('order',$designation)->first()->questions;
//            return Designation::with('questions')->with('questions.campaign')->where('order',$designation)->first()->questions;
        });

    }

    public function fetchChallengeDesignation($order)
    {
        $start = Setting::maxHpcs();

        return \Cache::remember('challengedesignation-' . $order, 1, function () use($order,$start) {
            return Designation::where('challenge', 1)->where('order', $order+$start)->first();
        });
    }

    public function getDesignation($designation)
    {
        return \Cache::remember('survey-' . $designation, 1, function () use($designation) {
            return Designation::with('questions')->where('order',$designation)->get()->first();
        });
    }

    public function fetchChallengeQuestions($designation)
    {
        if(!$designation) return null;

//        return \Cache::remember('challenge_designations' . $designation->id, 1, function () use ($designation) {
//            return Question::with('campaign')->with(['offer',function($q){
////                $q->
//            }])->where('designation_id', $designation->id)->get();
//        });
//
        return \Cache::remember('challenge_designations' . $designation->id, 1, function () use ($designation) {
            return Question::with('campaign')->with('offer')->where('designation_id', $designation->id)->get();
        });
    }

    public function fetchOffers()
    {
//        return \Cache::remember('offers', 1, function () {
            return Offer::with(['reviewsApproved'])
                ->where('is_active',1)
                ->where('general',1)
                ->orderBy('is_hpcs','DESC')
                ->orderBy('order','ASC')

                ->get();
//        });
    }

    public function money($user)
    {
        if(!$user)
        {
            return false;
        }
        $user->money_hpcs = collect($user->hpcs_taken)->where('is_Offer',0)->sum('cash');
        $user->money_hpcs_earn = collect($user->hpcs_taken)->where('is_Offer',0)->where('status',4)->sum('cash');
        $user->money_hpcs_paid = collect($user->hpcs_taken)->where('is_Offer',0)->where('status',5)->sum('cash');
        $user->money_hpcs_linkout = collect($user->hpcs_taken)->whereIn('is_Offer', [1,3,4,5])->sum('cash');
        $user->money_hpcs_linkout_earn = collect($user->hpcs_taken)->where('is_Offer',1)->where('status',3)->sum('cash');
        $user->money_hpcs_linkout_paid = collect($user->hpcs_taken)->where('is_Offer',1)->where('status',5)->sum('cash');
        $user->money_challenge = collect($user->challenge_taken)->whereIn('status',[4,5])->sum('cash');
        $user->money_challenge_earn = collect($user->challenge_taken)->where('status',4)->sum('cash');
        $user->money_challenge_paid = collect($user->challenge_taken)->where('status',5)->sum('cash');

        $user->money_linkout_offers = collect($user->offers_taken)->whereIn('status',[1,3,4,5])->sum('cash');
        $user->money_linkout_offers_submitted = collect($user->offers_taken)->whereIn('status',[1])->sum('cash');
        $user->money_linkout_offers_success = collect($user->offers_taken)->whereIn('status',[3])->sum('cash');

        $user->money_linkout_freebies = collect($user->freebies_taken)->whereIn('status',[1,3,4,5])->count('cash');
        $user->money_linkout_freebies_submitted = collect($user->freebies_taken)->whereIn('status',[1])->count('cash');
        $user->money_linkout_freebies_success = collect($user->freebies_taken)->whereIn('status',[3])->count('cash');

        if($user->registered)
        {
            $user->money_offers_pending = 0;
            $user->money_offers_submitted = collect($user->coregs)->whereIn('campaign.sponsor',1)->where('pivot.status',1)->sum('campaign.price');
            $user->money_offers_approved = collect($user->coregs)->where('campaign.sponsor',1)->where('pivot.status',3)->sum('pivot.cash');
            $user->money_offers_rejected = collect($user->coregs)->where('campaign.sponsor',1)->where('pivot.status',2)->sum('pivot.cash');
            $user->money_offers = $user->money_offers_submitted + $user->money_offers_approved + $user->money_linkout_offers;

            $user->count_freebies_pending = 0;
            $user->count_freebies_submitted = collect($user->coregs)->where('campaign.sponsor',0)->where('pivot.status',1)->count('campaign');
            $user->count_freebies_approved = collect($user->coregs)->where('campaign.sponsor',0)->where('pivot.status',3)->count('pivot');
            $user->count_freebies_rejected = collect($user->coregs)->where('campaign.sponsor',0)->where('pivot.status',2)->count('pivot');
            $user->count_freebies = $user->count_freebies_submitted + $user->count_freebies_approved + $user->money_linkout_freebies;
        }
        else
        {
            $user->money_offers_pending = collect($user->coregs)->where('campaign.sponsor',1)->sum('campaign.price');
            $user->money_offers_submitted = 0;
            $user->money_offers_approved = 0;
            $user->money_offers_rejected = 0;
            $user->money_offers = $user->money_offers_pending + $user->money_linkout_offers;

            $user->count_freebies_pending = collect($user->coregs)->where('campaign.sponsor',0)->count('campaign');
            $user->count_freebies_submitted = 0;
            $user->count_freebies_approved = 0;
            $user->count_freebies_rejected = 0;
            $user->count_freebies = $user->count_freebies_pending + $user->money_linkout_freebies;
        }

        return $user;
    }

    public function offer_params($offers,$user)
    {
        $url = 'https://engageiq.nlrtrk.com/?';
        $parameters = [];

        $format = $offers->map(function ($item, $key) use ($url,$user,$parameters)
        {
            $parameters['a'] = config('hpcs.cake_id');
            $parameters['c'] = $item->id;
            $parameters['s1'] = '';
            $parameters['s4'] = $user['id'];

            if($item->click_paid) {$parameters['p'] = 'c';}

            $parameters['s5'] = isset($user['email']) ? $user['email'] : null;
            $parameters['email'] = isset($user['email']) ? $user['email'] : null;
            $parameters['firstname'] = isset($user['first_name']) ? $user['first_name'] : null;
            $parameters['lastname'] =  isset($user['last_name']) ? $user['last_name'] : null;
            $parameters['first_name'] = isset($user['first_name']) ? $user['first_name'] : null;
            $parameters['last_name'] =  isset($user['last_name']) ? $user['last_name'] : null;
            $parameters['fname'] =  isset($user['first_name']) ? $user['first_name'] : null;
            $parameters['lname'] =  isset($user['last_name']) ? $user['last_name'] : null;
            $parameters['dobmonth'] = isset($user['dob']) ? Carbon::parse($user['dob'])->format('m') : null;
            $parameters['dobday'] = isset($user['dob']) ? Carbon::parse($user['dob'])->format('d') : null ;
            $parameters['dobyear'] =   isset($user['dob']) ? Carbon::parse($user['dob'])->format('y') : null;
            $parameters['zip'] = isset($user['zip']) ? $user['zip'] : null;
            $parameters['gender'] =  isset($user['gender']) ? $user['gender'] : null;
            $parameters['address'] =  isset($user['mail_address']) ? $user['mail_address'] : null;
            $parameters['state'] = isset($user['state']) ? $user['state'] : null;
            $parameters['phone1'] =  isset($user['phone1']) ? $user['phone1'] : null;
            $parameters['phone2'] =   isset($user['phone2']) ? $user['phone2'] : null;
            $parameters['phone3'] =  isset($user['phone3']) ?$user['phone3'] : null ;

            $item['cake_link'] = $url . http_build_query($parameters);
            return $item;
        });

        return $format;
    }
}