<?php

namespace App\HPCS\Session;

use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;

class Cookie
{
    protected $cookie;

    protected $request;

    const USER_ID = 'hpcs_user';

    /**
     * Guest constructor.
     * @param CookieJar $cookie
     * @param Request $request
     */
    public function __construct(CookieJar $cookie, Request $request)
    {
        $this->cookie = $cookie;
        $this->request = $request;
    }

    /**
     * set's a cookie to be non-http
     * expires in 2 months
     *
     * @param $value
     * @param $name
     */
    public function setTwoMonths($value, $name = self::USER_ID)
    {
        $this->cookie->queue($this->cookie->make($name, $value, 87600));
    }

    public function forget($name = self::USER_ID)
    {
        $this->cookie->queue($this->cookie->forget($name));
    }

    public function get($name = self::USER_ID)
    {
        return $this->request->cookie($name);
    }
}