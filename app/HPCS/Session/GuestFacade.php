<?php

namespace App\HPCS\Session;

use Illuminate\Support\Facades\Facade;

class GuestFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'guest';
    }
}