<?php

namespace App\HPCS\Session;

use App\HPCS\Entities\FreebiesUser;
use App\HPCS\Entities\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;

class Guest
{
    protected $cookie;

    protected $auth;

    /**
     * Guest constructor.
     * @param Cookie $cookie
     * @param AuthManager $auth
     */
    public function __construct(Cookie $cookie, AuthManager $auth)
    {
        $this->cookie = $cookie;
        $this->auth = $auth;
    }

    public function addCookieForUser()
    {
        $user = $this->createUser();

        $this->cookie->setTwoMonths($user->uuid);

        return $user;
    }

    public function user()
    {
        if ($user = $this->auth->guard('web')->user()) {
            return $user;
        }

        if ($uuid = $this->cookie->get()) {
            $user = User::byUuid($uuid)->first();

            return $this->checkIfRegistered($user);
        }
        return null;
    }

    /**
     * If the user has already registered
     * and the cookie still exists
     * we must delete it
     *
     * @param $user
     * @return null | User user
     */
    public function checkIfRegistered($user)
    {
        if (!$user) {
            return null;
        }

        if (!$user->registered) {
            return $user;
        }

        $this->deleteCookie();

        return null;
    }

    /**
     * Deletes the users cookie
     */
    public function deleteCookie()
    {
        $this->cookie->forget();
    }

    protected function createUser()
    {
        $user = User::create(['source_path'=>  session('user_origin')]);
        return $user;
    }
}