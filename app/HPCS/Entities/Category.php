<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['display', 'type'];

    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }
}