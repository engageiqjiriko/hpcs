<?php

namespace App\HPCS\Entities;

class Setting
{
    protected $guarded = ['id'];

    public static function maxHpcs()
    {
        return config('hpcs.max_hpcs');
    }

    public static function cakeId()
    {
        return config('hpcs.cake_id');
    }

    public static function revTracker()
    {
        return config('hpcs.rev_tracker');
    }

    public static function affiliateId()
    {
        return config('hpcs.eiq_affiliate_id');
    }
}