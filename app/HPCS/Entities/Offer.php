<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'id',
        'image',
        'name',
        'campaign_id',
        'description',
        'subtitle',
        'cash',
        'our_rating',
        'has_cash',
        'has_points',
        'has_freebie',
        'has_coupons',
        'has_sweeptakes',
        'points',
        'freebie_image',
        'hpcs_cash',
        'sweeptakes',
        'coupons',
        'link',
        'creative_id',
        'is_freebies',
        'is_sponsors',
        'is_hpcs',
        'is_active',
        'up_to',
        'click_paid'

    ];

//    protected $casts = [
//        'link' => 'array'
//    ];

    public $incrementing = false;

//    public function categories()
//    {
//        return $this->belongsToMany(Category::class);
//    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function reviewsApproved()
    {
        return $this->hasMany(Review::class)->where('approved',1);
    }
}