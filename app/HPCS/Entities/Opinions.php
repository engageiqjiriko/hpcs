<?php

namespace App\HPCS\Entities;


use Illuminate\Database\Eloquent\Model;

class Opinions extends Model
{
    protected $fillable = [
        'opinion',
        ];
}
