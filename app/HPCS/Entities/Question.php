<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'designation_id',
        'validators',
        'title',
        'heading',
        'content',
        'order',
        'lead_reactor',
        'campaign_id',
        'path',
        'polar',
        'status',
        'link_out',
        'offer_id'

    ];
    //protected $casts = ['validators' => 'array'];

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function leads()
    {
        return $this->hasMany(CoregLeads::class);
    }

}