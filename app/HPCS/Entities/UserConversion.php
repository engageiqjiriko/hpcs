<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class UserConversion extends Model
{
    protected $fillable = [
        'user_id', 'creative_id', 'cake_id',
        'conversion_id', 'click_id','executed',
        'email'
    ];

    protected $table = 'users_conversions';


    public function offer(){
        return $this->hasOne(Offer::class,'id','creative_id');
    }

    public function offerName(){
        return $this->hasOne(Offer::class,'id','creative_id')->select(['id', 'name']);
    }


}
