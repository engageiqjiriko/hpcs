<?php

namespace App\HPCS\Entities;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['offer_id','stars','comment'];

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public static function byCurrentUser($request)
    {
        $review = new static;
        $review->fill($request);
        $user = auth()->guard('web')->user();
        $review->user_id = $user->id;
        $review->author = ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);

        return $review;
    }
}