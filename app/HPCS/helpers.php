<?php

if (! function_exists('settings'))
{
    function settings($label)
    {
        return App\HPCS\Entities\Setting::cached()->{$label};
    }
}