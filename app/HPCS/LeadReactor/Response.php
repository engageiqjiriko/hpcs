<?php

namespace App\HPCS\LeadReactor;

class Response
{
    const STATUS_ACCEPTED = 'lead_received';

    const LEAD_ACCEPTED = 'Success';

    protected $status;
    protected $response;

    public function __construct($response)
    {
        $this->parse($response);
    }

    public function leadAccepted()
    {
        return $this->status==self::STATUS_ACCEPTED;
    }

    public function leadSuccess()
    {
        if($this->status==self::STATUS_ACCEPTED)
        {
            return $this->response->realtime_meaning==self::LEAD_ACCEPTED;
        }

        return false;
    }

    public function getFullResponse()
    {
        return $this->response;
    }

    protected function parse($response)
    {
        $response =  json_decode(trim($response->getBody(),'(;)\\r\n'));

        $this->status = $response->status;

        $this->response = $response;
    }
}