<?php

namespace App\Jobs;

use App\HPCS\Entities\FailedJobs;
use GuzzleHttp\Client as Client;
use App\HPCS\Entities\User;
use App\HPCS\LeadReactor\Mapper;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\HPCS\LeadReactor\Response as LeadReactorResponse;

class SendLeadsToLeadReactor extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $user;
    public $prepop;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user,$prepop=[])
    {
        $this->user = $user;
        $this->prepop = $prepop;
    }
    /**
     * Execute the job.
     * @return string
     */
    public function handle()
    {
        //use chunk to avoid timeout
        foreach($this->user->coregs->chunk(1) as $coreg)
        {
            $this->sendLead($coreg->first());
        }
    }
    protected function sendLead($lead)
    {
        //check if lead already sent

        if ($lead->pivot->status > 1) {
            return;
        }
        \Log::notice('Manual Send Leads -> user[' . $this->user->id . ']->question[' . $lead->pivot->question_id.']');

        try {
            $client = new Client();


            $mapper = new Mapper($lead->campaign, $this->user,$lead->pivot->additional_fields,$this->prepop);
            $url = $this->nlr_url().'/sendLead/?' . $mapper->buildQuery();
            $lead->pivot->nlr_submitted = $url; //nlr submitted

            $response = $client->get($url);

            \Log::notice('send url inside ->' . $url);

            $this->updateStatus($response, $lead , $lead->campaign->freebie);
        } catch (Exception $e) {

            \Log::emergency('Leads Failed reason:' . $e->getMessage());
            $lead->pivot->save();
            return $e->getMessage();
        }
    }

    protected function updateStatus($response,$coreg,$freebie)
    {
        $response = new LeadReactorResponse($response);
        $coreg->pivot->nlr_response = \GuzzleHttp\json_encode($response->getFullResponse()); //nlr response

        if(!$response->leadSuccess())
        {
            return $this->declinedCoreg($coreg);
        }

        $coreg->pivot->is_offers = 1;

        if($freebie==1)
        {
            $coreg->pivot->is_offers = 0;
        }

        $this->acceptedCoreg($coreg);

        $this->user->earnings()->incrementSponsors($coreg->campaign->price);
    }

    protected function acceptedCoreg($coreg)
    {
        $coreg->pivot->status = 3;
        $coreg->pivot->cash = $coreg->campaign->price;
        $coreg->pivot->save();
    }

    protected function declinedCoreg($coreg)
    {
        $coreg->pivot->status = 2;
        $coreg->pivot->cash = $coreg->campaign->price;
        $coreg->pivot->save();

    }

    public function nlr_url()
    {
        if(config('app.env')=='production')
        {
            return 'http://leadreactor.engageiq.com';
        }
        return 'http://testleadreactor.engageiq.com';
    }

}
