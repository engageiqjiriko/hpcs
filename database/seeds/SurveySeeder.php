<?php

use App\HPCS\Entities\Designation;
use App\HPCS\Entities\Setting;
use App\HPCS\Entities\Survey;
use Illuminate\Database\Seeder;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Survey::create(['label' => 'Default']); //not yet used

        Designation::create(
            [
                'label' => 'Question Set 1',
                'price' => 1,
                'no_of_questions' => 3,
                'survey_id' => 1,
                'order' => 1,
                'header_big' =>'Let\'s get started!',
                'header_big_class' =>'blue-font',
                'header_small' =>'Pick what you like:',
                'header_small_class' =>'blue-font',
                'challenge' => 0,
                'content' =>'',
            ]
        );

        Designation::create(
            [
                'label' => 'Dummy 1',
                'price' => 1,
                'order' => 2,
                'no_of_questions' => 5,
                'survey_id' => 1,
                'challenge' => 0,
                'header_big' =>'Great Job! You\'ve just earned $1!',
                'header_big_class' =>'green-font',
                'header_small' =>'Add a dollar more!',
                'header_small_class' =>'green-font',
                'content' =>'',
            ]
        );

        Designation::create(
            [
                'label' => 'Coreg1',
                'price' => 1,
                'order' => 3,
                'challenge' => 0,
                'no_of_questions' => 5,
                'survey_id' => 1,
                'header_big' =>'Nice! You\'ve now earned another $1!',
                'header_big_class' =>'green-font',
                'header_small' =>'Add a dollar more!',
                'header_small_class' =>'green-font',
                'content' =>'',
            ]
        );

        Designation::create(
            [
                'label' => 'Coreg2',
                'price' => 1,
                'order' => 4,
                'challenge' => 0,
                'no_of_questions' => 5,
                'survey_id' => 1,
                'header_big' =>'More for you! Another $1 in your earnings!',
                'header_big_class' =>'green-font',
                'header_small' =>'Add a dollar more!',
                'header_small_class' =>'green-font',
                'content' =>'',
            ]
        );


        for($i = 1; $i<= 30; $i++) {
            Designation::create(
                [
                    'label' => 'Day ' . $i,
                    'order' => $i,
                    'price' => 1,
                    'header_big' =>'Day ' . $i,
                    'header_big_class' =>'green-font',
                    'header_small' =>'Complete your challenge now!',
                    'header_small_class' =>'green-font',
                    'no_of_questions' => 5,
                    'survey_id' => 1
                ]
            );
        }
    }
}
