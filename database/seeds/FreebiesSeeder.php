<?php

use Illuminate\Database\Seeder;
use App\HPCS\Entities\Freebies;
use Illuminate\Support\Facades\DB;

class FreebiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Freebies::create(
            [
                'image' => '/offers/cpawall_pro_opinion.png',
                'name' => 'Pro Opinion',
                'subtitle' => 'Subtitle goes here',
                'description' => 'Freebies1',
                'cash' => 5,
                'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
                'id' => 2,
                'status'=>1
            ]
        );

        Freebies::create(
            [
                'image' => '/offers/cpawall_pro_opinion.png',
                'name' => 'Freebies2',
                'subtitle' => 'Subtitle goes here',
                'description' => 'Im your Freebies coming sooon',
                'cash' => 10,
                'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
                'id' => 1,
                'status'=>1
            ]
        );

        // DB::table('freebies')->insert(
        // [
        //     [
        //         'image' => '/offers/cpawall_pro_opinion.png',
        //         'name' => 'Pro Opinion',
        //         'subtitle' => 'Subtitle goes here',
        //         'description' => 'Freebies1',
        //         'cash' => 5,
        //         'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
        //         'id' => 2,
        //         'status'=>1
        //     ],
        //     [
        //         'image' => '/offers/cpawall_pro_opinion.png',
        //         'name' => 'Freebies2',
        //         'subtitle' => 'Subtitle goes here',
        //         'description' => 'Im your Freebies coming sooon',
        //         'cash' => 10,
        //         'our_rating' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et tempora, molestiae voluptate nostrum corrupti!',
        //         'id' => 1,
        //         'status'=>1
        //     ]
        // ]
        // );
    }

}
