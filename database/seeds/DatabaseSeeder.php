<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//       $this->call(SurveySeeder::class);
//       $this->call(QuestionSeeder::class);
//        $this->call(CampaignSeeder::class);
//        $this->call(CampaignSeeder3::class);
       $this->call(AdminSeeder::class);
//       $this->call(FreebiesSeeder::class);
    }
}
