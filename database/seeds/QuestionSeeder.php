<?php

use App\HPCS\Entities\Campaign;
use App\HPCS\Entities\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create(
            [
                'designation_id' => 1,
                'order' => 1,
                'title' => 'Product Reviews',
                'polar' => 0,
                'content' => 'How much do you want to get paid?'
            ]
        );

        Question::create(
            [
                'designation_id' => 1,
                'order' => 2,
                'title' => 'Online Surveys',
                'polar' => 0,
                'content' => 'How much do you want to get paid?'
            ]
        );

        Question::create(
            [
                'designation_id' => 1,
                'order' => 3,
                'title' => 'In Person Survey',
                'polar' => 0,
                'content' => 'How much do you want to get paid?'
            ]
        );

        for ($i = 1; $i <= 5; $i++)
        {
            Question::create(
                [
                    'designation_id' => 2,
                    'order' => $i,
                    'title' => 'Dummy question 2 ' . $i,
                    'content' => 'How much do you want to get paid?'
                ]
            );
        }



        //campaign questions
        $campaign = Campaign::create([
            'deal' => 'Jobs2shop co-reg',
            'eiq_campaign_id' => '7',
            'description' => 'Jobs2shop test campaign',
            'image' => 'images/campaigns/jobs2shop.png',
            'price' => 3,
            'fields' => ['eiq_email','first_name','last_name','ip']
        ]);

        Question::create(
            [
                'designation_id' => 3,
                'order' => 1,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Make shopping your job!',
                'content' => 'Do you want to become a member of Jobs2Shop Rewards Panel to get compensated up to $1 per email you read and up to $85 per product?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Vindale Research',
            'eiq_campaign_id' => 12,
            'description' => 'Vindale test campaign',
            'image' => 'images/campaigns/vindaleresearch.png',
            'price' => 10,
            'dob_format' => 'm/d/Y',
            'fields' => ['eiq_email','first_name','last_name','zip','date_of_birth','gender','ip','createDate']
        ]);

        Question::create(
            [
                'designation_id' => 3,
                'order' => 2,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Join to be one of the finest minds in consumer research and change the world one product at a time.',
                'content' => 'Are you ready to share your opinion and get paid $5 - $75 for each completed survey?'
            ]
        );


        $campaign = Campaign::create([
            'deal' => 'MySurvey Coreg SOI',
            'eiq_campaign_id' => 2,
            'description' => 'My survey test campaign',
            'image' => 'images/campaigns/mysurvey.png',
            'price' => 3,
            'fields' => ['eiq_email','first_name','last_name']
        ]);

        Question::create(
            [
                'designation_id' => 3,
                'order' => 3,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Join MySurvey today and you could win up to $2000.',
                'content' => 'Fill out a few surveys and redeem your points for $50 through your paypal. Register now?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Opinion Outpost',
            'eiq_campaign_id' => 129,
            'description' => 'Opinion Outpost test campaign',
            'image' => 'images/campaigns/opinionoutpost.png',
            'price' => 4,
            'fields' => ['eiq_email','first_name','last_name','gender']
        ]);

        Question::create(
            [
                'designation_id' => 3,
                'order' => 4,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Earn extra cash and popular gift card with Opinion Outpost today!',
                'content' => 'Earn CASH and other rewards for your time?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Fusion Cash',
            'eiq_campaign_id' => 20,
            'description' => 'Fusion Cash test campaign',
            'image' => 'images/campaigns/fusioncash.png',
            'price' => 2,
            'fields' => ['eiq_email','first_name','last_name','zip','ip','postedstamp']
        ]);

        Question::create(
            [
                'designation_id' => 3,
                'order' => 5,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Be rewarded for watching videos, reading emails, taking surveys and much more. Join now!',
                'content' => 'Are you ready to get $5 sign-up bonus and more free money today?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Survey 4 Bucks',
            'eiq_campaign_id' => 30,
            'description' => 'Survey 4 Bucks test campaign',
            'image' => 'images/campaigns/surveys4bucks.png',
            'price' => 2,
            'fields' => ['eiq_email','first_name','last_name','ip','timestamp']
        ]);

        Question::create(
            [
                'designation_id' => 4,
                'order' => 1,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => 'Get up to $25 per survey',
                'content' => 'Do you want to get $1 - $25 to take online surveys; $10 - $50 to shop and rate your customer experience; and get $30 - $150 to participate in discussion groups?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'MindsPay SOI Coreg',
            'eiq_campaign_id' => 6,
            'description' => 'MindsPay SOI Coreg',
            'image' => 'images/campaigns/mindspay.png',
            'price' => 2,
            'fields' => ['eiq_email','first_name','last_name','dob','ip','today', 'rev_tracker_map']
        ]);

        Question::create(
            [
                'designation_id' => 4,
                'order' => 2,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => '$6 Sign Up Bonus',
                'content' => 'Do you want to earn up to $100 for each completed survey or review? Plus, earn up to $0.50 for every email you read?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Inbox Pays',
            'eiq_campaign_id' => 14,
            'description' => 'Inbox Pays',
            'image' => 'images/campaigns/inboxpays.png',
            'price' => 2,
            'dob_format' => 'Y-m-d',
            'fields' => ['eiq_email','first_name','last_name','zip','date_of_birth','ip','dj', 'rev_tracker_map']
        ]);

        Question::create(
            [
                'designation_id' => 4,
                'order' => 3,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => '$5 Sign Up Bonus',
                'content' => 'Register for FREE and InboxPays will pay you $5 for signing up! Complete trial offers, read emails and play online games to earn Instant cash offers?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Survey Monsters',
            'eiq_campaign_id' => 23,
            'description' => 'Survey Monsters',
            'image' => 'images/campaigns/surveymonster.png',
            'price' => 2,
            'fields' => ['eiq_email','first_name','last_name','zip','dob','gender','ip','dj']
        ]);

        Question::create(
            [
                'designation_id' => 4,
                'order' => 4,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => '',
                'content' => 'Would you like to make $250.00/hr doing surveys from home?'
            ]
        );

        $campaign = Campaign::create([
            'deal' => 'Panda Research',
            'eiq_campaign_id' => 22,
            'description' => 'Panda Research',
            'image' => 'images/campaigns/pandaresearch.png',
            'price' => 2,
            'dob_format' => 'm/d/Y',
            'fields' => ['eiq_email','first_name','last_name','date_of_birth','ip','dj','rev_tracker_map']
        ]);

        Question::create(
            [
                'designation_id' => 4,
                'order' => 5,
                'lead_reactor' => 1,
                'campaign_id' => $campaign->id,
                'title' => '',
                'content' => 'Would you like to start earning $5 - $75 per completed survey?'
            ]
        );


        //day 1 dummy questions
        for($x = 5; $x <=33; $x++)
        {
            for ($i = 1; $i <= 5; $i++)
            {
                Question::create(
                    [
                        'designation_id' => $x,
                        'order' => $i,
                        'title' => '30 day challenge Day' . ($x-5) . ' no.' . $i,
                        'content' => 'How much do you want to get paid?'
                    ]
                );
            }
        }

    }
}
