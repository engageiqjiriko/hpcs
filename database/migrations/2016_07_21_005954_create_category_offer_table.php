<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_offer', function (Blueprint $table) {
            $table->integer('offer_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->primary(['offer_id','category_id']);

            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('CASCADE');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_offer');
    }
}
