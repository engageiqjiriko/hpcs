<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
//            $table->renameColumn('id', 'id_stnk');
            $table->integer('creative_id')->default(0)->change();
            $table->boolean('has_sweeptakes')->default(0)->change();
            $table->boolean('sweeptakes')->default(0)->change();
            $table->boolean('has_coupons')->default(0)->change();
            $table->boolean('coupons')->default(0)->change();

            $table->boolean('image')->nullable()->change();

//            $table->increments('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
    }
}
