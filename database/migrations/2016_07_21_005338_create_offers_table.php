<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->string('image');
            $table->text('description');
            $table->text('our_rating');
            $table->string('name');
            $table->decimal('cash',5,2)->nullable();
            $table->string('freebie_image')->nullable();
            $table->text('subtitle');
            $table->boolean('has_freebie')->default(0);
            $table->boolean('has_cash')->default(1);
            $table->boolean('has_points')->default(0);
            $table->smallInteger('points')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offers');
    }
}
