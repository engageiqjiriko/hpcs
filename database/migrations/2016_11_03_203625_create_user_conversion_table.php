<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConversionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_conversions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('creative_id')->nullable();
            $table->integer('cake_id')->nullable();
            $table->integer('conversion_id')->nullable();
            $table->integer('click_id')->nullable();
            $table->boolean('executed')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('users_conversions');
    }
}
