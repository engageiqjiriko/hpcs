<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designation_id')->unsigned()->index();
            $table->text('validators')->nullable();
            $table->string('title')->nullable();
            $table->string('heading')->nullable();
            $table->text('content')->nullable();
            $table->smallInteger('order')->unsigned();
            $table->boolean('lead_reactor')->default(0);
            $table->integer('campaign_id')->nullable();
            $table->text('path')->nullable();
            $table->boolean('polar')->default(1); //yes or no
            $table->timestamps();

            $table->foreign('designation_id')->references('id')->on('designations')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
