<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->text('description')->nullable();
            $table->string('header_big')->nullable();
            $table->string('header_big_class')->nullable();
            $table->string('header_small')->nullable();
            $table->string('header_small_class')->nullable();
            $table->text('content')->nullable();
            $table->string('content_class')->nullable();
            $table->smallInteger('no_of_questions')->default(10)->unsigned();
            $table->boolean('challenge')->default(1);
            $table->smallInteger('price')->unsigned();
            $table->integer('survey_id')->unsigned()->nullable();
            $table->smallInteger('order')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('designations');
    }
}
