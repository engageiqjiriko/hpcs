<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deal');
            $table->integer('eiq_campaign_id');
            $table->text('description');
            $table->boolean('freebie')->default(0);
            $table->boolean('sponsor')->default(0);
            $table->string('image')->nullable();
            $table->text('fields')->nullable();
            $table->string('dob_format')->nullable();
            $table->string('freebie_image')->nullable();
            $table->decimal('price',9,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }
}
