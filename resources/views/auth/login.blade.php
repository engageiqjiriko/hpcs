<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>HPCS | Login</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="_csrftoken" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link href="/css/login-component.css" rel="stylesheet" type="text/css" />
    <link href="/css/login.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- Begin Favicons for html and mobile browsers -->
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="HighPayingCashSurveys">
    <meta name="application-name" content="HighPayingCashSurveys">
    <meta name="theme-color" content="#ffffff">
    <!-- End Favicons for html and mobile browsers -->
</head>
<body class="login">
<div class="logo">
    <a href="#">
        <img src="/images/logo.png" alt="hpcs" width="200px">
    </a>
</div>
<div class="content">


    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="/admin/login" method="post" novalidate="novalidate">
        {!! csrf_field() !!}
        <h3 class="form-title font-blue">Admin Login</h3>
        @if ($errors->any())
            <div class="alert alert-danger ">
                <button class="close" data-close="alert"></button>
                <span>Invalid email/password combination.</span>
            </div>
        @endif

        @if (session()->has('flash_notification.message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('flash_notification.message')}}
            </div>
        @endif
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" autofocus> </div>
        <div class="form-group">
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"> </div>
        <div class="form-actions">
            <button type="submit" class="btn blue uppercase">Login</button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="true">Remember
                <span></span>
            </label>
        </div>

    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <!-- <form class="forget-form" action="index.html" method="post" novalidate="novalidate">
        <h3 class="font-green">Forget Password ?</h3>
        <p> Enter your e-mail address below to reset your password. </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"> </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
            <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    </form> -->
    <!-- END FORGOT PASSWORD FORM -->
</div>
<div class="copyright"> 2016 © HPCS. Admin Dashboard Portal. </div>

<!-- BEGIN CORE PLUGINS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- END CORE PLUGINS -->
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
</body>