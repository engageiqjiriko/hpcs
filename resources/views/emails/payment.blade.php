<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Creative</title>
    <style type="text/css">
        #templateContainerHeader {
            font-size: 14px;
            padding-top: 2.429em;
            padding-bottom: 0.929em;
        }

        #templateContainerMiddle {
            border: 1px solid #e2e2e2;
            border-top: 5px solid #5098EA;
            width: 50%;
        }

        .bodyContent {
            color: #505050;
            font-family: Helvetica;
            font-size: 14px;
            line-height: 150%;
            padding-top: 0.5em;
            padding-right: 3.5em;
            padding-left: 3.5em;
            padding-bottom: 0.714em;
            text-align: left;
        }

        .bodyContentCenter {
            color: #505050;
            font-family: Helvetica;
            font-size: 14px;
            line-height: 150%;
            padding-top: 0em;
            padding-right: 3.5em;
            padding-left: 3.5em;
            padding-bottom: 3.0em;
            text-align: center;
        }

        h2 {
            color: #2e2e2e;
            display: block;
            font-family: Helvetica;
            font-size: 1.3em;
            line-height: 1.455em;
            font-style: normal;
            font-weight: normal;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 15px;
            margin-left: 0;
            text-align: center;
        }

        p {
            color: #545454;
            display: block;
            font-family: Helvetica;
            font-size: 16px;
            line-height: 1.500em;
            font-style: normal;
            font-weight: normal;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 15px;
            margin-left: 0;
            text-align: left;
        }

        a {
            text-decoration: none;
        }

        a.blue-btn {
            background: #5098ea;
            display: inline-block;
            color: #FFFFFF;
            border-top:10px solid #5098ea;
            border-bottom:10px solid #5098ea;
            border-bottom:10px solid #5098ea;
            border-left:20px solid #5098ea;
            border-right:20px solid #5098ea;
            text-decoration: none;
            font-size: 14px;
            margin-top: 1.0em;
            border-radius: 3px 3px 3px 3px;
            background-clip: padding-box;
        }

        #bodyCellFooter {
            margin: 0;
            padding: 0;
            width: 100% !important;
            padding-top: 39px;
            padding-bottom: 15px;
        }

        table {
            border-collapse: collapse !important;
        }

        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .hide {display: none!important;}
            body[yahoo] .buttonwrapper {background-color: transparent!important;}
            body[yahoo] .button {padding: 0px!important;}
            body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
            body[yahoo] .unsubscribe { font-size: 14px; display: block; margin-top: 0.714em; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important;}
        }
        /*@media only screen and (min-device-width: 601px) {
          .content {width: 600px !important;}
        }*/
        @media only screen and (max-width: 480px) {
            #templateContainerMiddle {
                width: 90%;
            }

            h2 {
                font-size: 80%;
            }

            h2{
                font-size:30px !important;
            }
            p {
                font-size: 18px !important;
            }
            .bodyContent { padding-bottom: 2.286em !important; }
            .bodyContent {
                padding: 6% 5% 1% 6% !important;
            }
            td[class="bodyContentCenter"] {
                padding: 6% 6% 6% 6% !important;
            }
        }
    </style>
</head>
<body yahoo bgcolor="#ffffff">
<table style="width: 100%;" bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td align="center" valign="top">
            <!-- BEGIN BODY // -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainerMiddle">
                <tbody>
                <tr>
                    <td valign="top" class="bodyContent" mc:edit="body_content">
                        <img src="http://live.highpayingcashsurveys.com/images/logo.png" alt="">
                        <br>
                        <br>
                        <br>

                        <p>Hi {{ $first_name  }},</p><br>
                        <p>
                            We have sent out your payment of <strong style="color: #0071bc;">${{ $value }}</strong> today. It will take 3-5 business for it to reach you. In the meantime, please check <strong><a href="http://live.highpayingcashsurveys.com/" target="_blank">highpayingcashsurveys.com</a></strong> to earn more.
                        </p>
                        <br><br>
                        <p>
                            Sincerely,<br>
                            HPCS Team
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <table>
                <tbody>
                <tr>
                    <td>
                        <p style="margin-top: 20px; font-size: 12px;"><a style="color: #0071bc;" href="http://live.highpayingcashsurveys.com/" target="_blank">live.highpayingcashsurveys.com</a></p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- // END BODY -->
        </td>
    </tr>
</table>
</body>
</html>