<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>HPCS | @yield('title')</title>
	@yield('styles')
</head>
<body>
	@yield('content')
	@yield('scripts')
</body>
</html>