@extends('backend.layouts.main',['title' => 'survey.coreg.create'])
@section('content')
@section('title', 'Add Survey Question')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Co-Reg Question</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Co-Reg Question</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-road"></i>
          <span class="caption-subject bold uppercase"> Add Co-Reg Question</span>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6">
            <form action="" role="form">

              <div class="form-group">
                <label for="">Question Order</label>
                <select name="order" id="order" class="form-control input-lg" required="required">
                  @for($i = 0; $i < 10; $i++)
                  <option>{{ $i }}</option>
                  @endfor
                </select>
              </div>

              <div class="form-group">
                <label for="">Title</label>
                <input type="text" name="title" class="form-control input-lg" required>
              </div>

              <div class="form-group">
                <label for="">Header</label>
                <input type="text" name="header" class="form-control input-lg" required>
              </div>

              <div class="form-group">
                <label for="">Description</label>
                <input type="text" name="desc" class="form-control input-lg" required>
              </div>

              <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;"></div>
                  <div>
                      <span class="btn blue btn-lg btn-file">
                          <span class="fileinput-new"> Select image </span>
                          <span class="fileinput-exists"> Change </span>
                          <input type="hidden" value="" name="..."><input type="file" name=""> </span>
                      <a href="javascript:;" class="btn btn-lg red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
              </div>

          </div>
          <div class="col-md-6">
                <div class="mt-checkbox-list">
                  <label class="mt-checkbox"> Paid
                    <input type="checkbox" value="1" name="paid">
                    <span></span>
                  </label>
                </div>

                <div class="form-group">
                  <label for="">From our Sponsors</label>
                  <input type="text" name="sponsors" class="form-control input-lg" required>
                </div>

                <div class="mt-checkbox-list">
                  <label class="mt-checkbox"> Freebie
                    <input type="checkbox" value="1" name="freebie">
                    <span></span>
                  </label>
                </div>

                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;"></div>
                  <div>
                      <span class="btn blue btn-lg btn-file">
                          <span class="fileinput-new"> Select freebie image </span>
                          <span class="fileinput-exists"> Change </span>
                          <input type="hidden" value="" name="..."><input type="file" name=""> </span>
                      <a href="javascript:;" class="btn btn-lg red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
    <button type="button" class="btn btn-lg blue">
    <i class="fa fa-save"></i>  Save
  </button>
  </div>
</div>

@stop