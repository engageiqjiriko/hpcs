@extends('backend.layouts.main',['title' => 'survey.question.create'])
@section('content')
@section('title', 'Add Survey Question')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Survey Question</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Survey Question</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-road"></i>
          <span class="caption-subject bold uppercase"> Add Survey Question</span>
        </div>
        <div class="actions">
          <a href="/question-list" class="btn btn-default">
           <i class="fa fa-list"></i> Survey List
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6">
            <form action="" role="form">
              <div class="form-group">
                <label for="">Survey</label>
                <select name="survey" id="input" class="form-control input-lg" required="required">
                  <option value="">Day 1</option>
                  <option value="">Day 2</option>
                  <option value="">Day 3</option>
                  <option value="">Day 4</option>
                  <option value="">Day 5</option>
                </select>
              </div>
              <div class="form-group">
                <label for="">Question</label>
                <textarea name="question" id="inputQuestion" class="form-control input-lg description" rows="3" required="required"></textarea>
              </div>
              <div class="form-group">
                <label for="">Question Order</label>
                <select name="order" id="input" class="form-control input-lg" required="required">
                  <option value="">1</option>
                  <option value="">2</option>
                  <option value="">3</option>
                  <option value="">4</option>
                  <option value="">5</option>
                </select>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
    <button type="button" class="btn btn-lg blue">
    <i class="fa fa-save"></i>  Save
  </button>
  </div>
</div>

@stop