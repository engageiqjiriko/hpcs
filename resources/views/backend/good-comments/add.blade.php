@extends('backend.layouts.main')
@section('content')
@section('title', 'Add Good Comment')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Good Comment</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Good Comment</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
      <div class="caption">
        <i class="fa fa-comments"></i>
        <span class="caption-subject bold uppercase"> Add Good Comment Form</span>
      </div>
    </div>
    <div class="portlet-body">
    	<div class="row">
    		<div class="col-md-6">
    			<form action="" role="form">
		    		<div class="form-group">
			    		<label for="">Survey</label>
			    		<select class="form-control input-lg" name="survey">
			    			@for($i = 1; $i <= 10; $i++)
			    			<option>Day {{ $i }}</option>
			    			@endfor
			    		</select>
			    	</div>
			    	<div class="form-group">
			    		<label for="">Comment</label>
			    		<textarea class="form-control input-lg description" name="comment" placeholder="Comment"></textarea>
			    	</div>
    		</div>
    	</div>
    </div>
		</div>
	</div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
@stop