@extends('backend.layouts.main')
@section('content')
@section('title', 'Zipcode List')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Zipcode List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Zipcode List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-barcode"></i>
        <span class="caption-subject bold uppercase"> Zipcode List</span>
      </div>
      <div class="actions">
      	<a href="/admin/zipcode/create" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Zipcode
      	</a>
      </div>
    </div>
    <div class="portlet-body">
    	<table class="table table-hover">
    		<thead>
    			<tr>
    				<th>Sr.#</th>
    				<th>Zipcode</th>
    				<th>Zip Type</th>
    				<th>City</th>
    				<th>State</th>
    				<th>Country</th>
    				<th>Area Codes</th>
    				<th>Country</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    			@for($i = 1; $i<=10; $i++)
    				<tr>
    					<td>1</td>
    					<td>56554</td>
    					<td>PO BOX</td>
    					<td>Hyder</td>
    					<td>AK</td>
    					<td>Prince of Wales-Outer Ketchikan Borough</td>
    					<td>907</td>
    					<td>US</td>
    					<td></td>
    				</tr>
    			@endfor
    		</tbody>
    	</table>
    	<div class="text-right">
			  <ul class="pagination pagination-sm">
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-left"></i>
			      </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 1 </a>
			    </li>
			    <li class="active">
			      <a href="javascript:;"> 2 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 3 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 4 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 5 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 6 </a>
			    </li>
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-right"></i>
			      </a>
			    </li>
			  </ul>
			</div>
    </div>
   </div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop