@extends('backend.layouts.main')
@section('content')
@section('title', 'Add Zipcode')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Zipcode</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Zipcode</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN ADD ZIPCODE MAIN -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-barcode"></i>
          <span class="caption-subject bold uppercase"> Add Zipcode Form</span>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6">
              <form role="form">
                <div class="form-group">
                  <label>Zipcode</label>
                  <input type="text" name="zip" class="form-control input-lg" placeholder="Zipcode" required>
                </div>
                <div class="form-group">
                  <label>Zip Type</label>
                  <input type="text" name="type" class="form-control input-lg" placeholder="Zip Type" required>
                </div>
                <div class="form-group">
                  <label>City</label>
                  <input type="text" name="city" class="form-control input-lg" placeholder="City" required>
                </div>
                <div class="form-group">
                  <label>Acceptable Cities</label>
                  <input type="text" name="accept" class="form-control input-lg" placeholder="Acceptable Cities" required>
                </div>
                <div class="form-group">
                  <label>Unacceptable Cities</label>
                  <input type="text" name="unaccept" class="form-control input-lg" placeholder="Unacceptable Cities" required>
                </div>
                <div class="form-group">
                  <label>County</label>
                  <input type="text" name="county" class="form-control input-lg" placeholder="County" required>
                </div>
                <div class="form-group">
                  <label>State</label>
                  <input type="text" name="state" class="form-control input-lg" placeholder="State" required>
                </div>
                <div class="form-group">
                  <label>Timezone</label>
                  <input type="text" name="timezone" class="form-control input-lg" placeholder="Timezone" required>
                </div>
          </div>
          <div class="col-md-6">
                <div class="form-group">
                  <label>Latitude</label>
                  <input type="number" name="lattitude" class="form-control input-lg">
                </div>
                <div class="form-group">
                  <label>Longitude</label>
                  <input type="number" name="longitude" class="form-control input-lg">
                </div>
                <div class="form-group">
                  <label>World Region</label>
                  <input type="text" name="wregion" class="form-control input-lg" placeholder="World Region">
                </div>
                <div class="form-group">
                  <label>Country</label>
                  <input type="text" name="country" class="form-control input-lg" placeholder="Country">
                </div>
                <div class="form-group">
                  <label>Decommissioned</label>
                  <input type="number" name="decom" class="form-control input-lg">
                </div>
                <div class="form-group">
                  <label>Estimated Population</label>
                  <input type="number" name="epopulation" class="form-control input-lg">
                </div>
                <div class="form-group">
                  <label>Notes</label>
                  <textarea name="notes" class="form-control input-lg description" placeholder="Notes"></textarea>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END ADD ZIPCODE MAIN -->
@stop