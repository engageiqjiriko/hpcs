@extends('backend.layouts.main')
@section('content')
@section('title', 'Add Feedback')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Add Feedback</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Feedback</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-comment-o"></i>
              <span class="caption-subject bold uppercase"> Add Feedback Form</span>
            </div>
          </div>
          <div class="portlet-body">
            <div class="row">
              <div class="col-md-6">
                <form action="" role="form">
                  <div class="form-group">
                    <label for="">Message</label>
                    <textarea class="form-control description" name="message" placeholder="Message" style="height: 282px;"></textarea>
                  </div>
              </div>
            </div>
          </div>
      </div>
	</div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
@stop