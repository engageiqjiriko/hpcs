@extends('backend.layouts.main',['title' => 'survey.list.local'])
@section('content')
@section('title', 'Survey List')

@section('styles')
@stop
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Survey List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Add Survey</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
	  <div class="portlet box blue">
	  	<div class="portlet-title">
      <div class="caption">
        <i class="fa fa-road"></i>
        <span class="caption-subject bold uppercase"> Survey List <small>(Local Search)</small></span>
      </div>
      <div class="actions">
      	<a href="/admin/survey/create" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Survey
      	</a>
      </div>
    </div>
    <div class="portlet-body">
			<table id="surveys-local-table" class="table table table-striped" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="5%">Sr. #</th>
						<th width="20%">Image</th>
						<th width="15">Name</th>
						<th width="15">Description</th>
						<th width="5%">Cake</th>
						<th width="5%">Type</th>
						<th width="5%">Cash</th>
						<th width="5%">Hpcs</th>
						<th width="5%">Reward</th>
						<th width="10%">Created</th>
						<th width="10%">Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="offer in offers.data">
						<td>@{{ offer.id }}</td>
						<td><img :src="offer.image" width="100%" alt=""></td>
						<td>@{{ offer.name }}</td>
						<td>@{{ offer.description }}</td>
						<td> @{{ offer.cake_id }}</td>
						<td>@{{ offer.is_sponsors | offerFilter }}</td>
						<td>@{{ offer.cash | currency }}</td>
						<td>@{{ offer.is_hpcs | hpcsFilter }}</td>
						<td>@{{ offer.hpcs_cash | currency}}</td>
						<td>@{{ offer.created_at }}</td>
						<td><button>haha</button></td>
					</tr>
				</tbody>
			</table>
			</div>
	  <!-- END SAMPLE FORM PORTLET -->
		</div>
	</div>
</div>

@stop

@section('scripts')


	<script src="/js/backend/common.js"></script>
	<script type="text/javascript">

		new Vue ({

			el: 'body',

			data: {

				offers: [],

			},

			ready: function () {
				this.initialize();
			},

			methods: {

				initialize: function () {
					var self = this;
					$.get('/admin/survey/offers/list', function (response) {
						self.offers = response.data;
					})
				}
			},

			filters: {
				offerFilter : function(data)
				{
					if(data==1)
					{
						return 'Sponsor';
					}
					return 'Freebies';
				},

				hpcsFilter : function (data)
				{
					if(data==1)
					{
						return 'Yes';
					}
					return 'No';
				}
			}
		});





















//			$('#surveys-local-table').DataTable({
//			processing: true,
//			serverSide: true,
//			responsive: true,
//			ajax: '/admin/survey/local/data',
//			columns: [
//				{ data: 'id', name: 'id' },
//				{ data: 'image', name: 'image' },
//				{ data: 'name', name: 'name' },
//				{ data: 'description', name: 'description' },
//				{ data: 'cash', name: 'cash' },
//				{ data: 'created_at', name: 'created_at', searchable: false },
//				{ data: 'action','className': 'text-center', name: 'action', orderable: false,searchable: false, 'width': '15%' },
//			],
//			"oLanguage": {
//				"sLengthMenu": "Entries per page: _MENU_",
//			},
//		});
//		$('<button id="refresh" type="button" class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>').appendTo('div.dataTables_filter');
//
//		$('#refresh').click(function(){
//			$('#users-table').dataTable().api().draw();
//		});
	</script>
@stop