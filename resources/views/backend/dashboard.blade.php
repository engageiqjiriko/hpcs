@extends('backend.layouts.main',['title' => 'dashboard']);
@section('content')
@section('title', 'Dashboard')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li>
      <a href="javascript:;">Home</a>
    </li>
    <li class="active">Dashboard</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN PAGE BASE CONTENT -->
<!-- BEGIN DASHBOARD STATS 1-->
  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
    <a class="dashboard-stat dashboard-stat-v2 green-jungle" href="/admin/user">
      <div class="visual">
        <i class="fa fa-user"></i>
      </div>
      <div class="details">
        <div class="number">
          <span data-counter="counterup">{{$users}}</span>
        </div>
        <div class="desc"> Users </div>
      </div>
    </a>
  </div>
  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
    <a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="/admin/survey">
      <div class="visual">
        <i class="fa fa-road" aria-hidden="true""></i>
      </div>
      <div class="details">
        <div class="number">
          <span data-counter="counterup">0</span>
        </div>
        <div class="desc"> Surveys </div>
      </div>
    </a>
  </div>

<!--  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
   <a class="dashboard-stat dashboard-stat-v2 grey" href="/admin/question">
     <div class="visual">
       <i class="fa fa-question" aria-hidden="true"></i>
     </div>
     <div class="details">
       <div class="number">
       </div>
       <div class="desc"> Questions </div>
     </div>
   </a>
 </div> -->

  <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
    <a class="dashboard-stat dashboard-stat-v2 yellow" href="/admin/review">
      <div class="visual">
        <i class="fa fa-star" aria-hidden="true"></i>
      </div>
      <div class="details">
        <div class="number">
            <span data-counter="counterup">{{$reviews}}</span>
        </div>
        <div class="desc"> Reviews </div>
      </div>
    </a>
  </div>

<div class="clearfix"></div>

<div class="row">
    {{--
    <div class="col-md-3">
        <!-- BEGIN Portlet PORTLET-->
          <div class="portlet light portlet-fit">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-comments font-blue"></i>
                <span class="caption-subject bold font-grey-gallery uppercase"> Comments </span>
              </div>
              <div class="tools">
                <a href="" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
                <a href="" class="remove" data-original-title="" title=""> </a>
              </div>
            </div>
            <div class="portlet-body">
               <div class="row">
                  <div class="col-md-12">
                     <a class="dashboard-stat dashboard-stat-v2 bordered font-red" href="javascript:;">
                      <div class="visual">
                        <i class="fa fa-user" aria-hidden="true"></i>
                      </div>
                      <div class="details">
                        <div class="number">
                         <span data-counter="counterup" data-value="1349">65</span>
                        </div>
                        <div class="desc"> New </div>
                      </div>
                    </a>
                 </div>
              </div>
              <div class="spacer"></div>
              <div class="row">
                  <div class="col-md-12">
                     <a class="dashboard-stat dashboard-stat-v2 bordered font-green-jungle" href="javascript:;">
                      <div class="visual">
                        <i class="fa fa-book" aria-hidden="true"></i>
                      </div>
                      <div class="details">
                        <div class="number">
                         <span data-counter="counterup" data-value="1349">14</span>
                        </div>
                        <div class="desc"> Approved </div>
                      </div>
                    </a>
                 </div>
              </div>
              <div class="spacer"></div>
              <div class="row">
                  <div class="col-md-12">
                     <a class="dashboard-stat dashboard-stat-v2 bordered" href="javascript:;">
                      <div class="visual">
                        <i class="fa fa-inbox" aria-hidden="true"></i>
                      </div>
                      <div class="details">
                        <div class="number">
                         <span data-counter="counterup" data-value="1349">1</span>
                        </div>
                        <div class="desc"> Pending </div>
                      </div>
                    </a>
                 </div>
              </div>
            </div>
          </div>
        <!-- END GRID PORTLET-->
    </div>--}}
    <!-- END BEGIN PORTLET -->
</div>
<!-- END DASHBOARD STATS 1-->
@stop
@section('scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>

<script>

  new Vue({

    el:'body',


    methods : {

      test : function(){


      }
    }
  })



</script>
@stop

