@extends('backend.layouts.main')
@section('content')
@section('title', 'Bad Comment Filter')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Bad Comment Filter</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Bad Comment Filter</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-filter"></i>
          <span class="caption-subject bold uppercase"> Bad Comment Filter</span>
        </div>
        <div class="actions">
          <a href="/bad-comments-list" class="btn btn-default">
           <i class="fa fa-list"></i> Bad Comments List
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6">
          <form action="" role="form">
            <div class="form-group">
              <label for="">Please provide comment strings and sentences with comma(,) separated.</label>
              <textarea class="form-control description" style="height: 200px">
bad, scam, cheat, not good, not like, not authentic, ashamed, not serious, harmful, harm, unpleasant, severe, inappropriate, corrupt, badly behaved, worthless, not valid, invalid, fake, false, bogus, fraudulent, awful, worthless, counterfeit, fake, false, bogus, useless,  amateurish, not suitable, suspicious, off, dreadful, rotten, terrible, guilty, feel sorry, spoiled, damaging, detrimental, injurious, hurtful, inimical, destructive, ruinous, deleterious; unhealthy, unwholesome, dishonest, fraud, racket, trick, con, hustle, flimflam, bunco, grift, gyp, shakedown, deceive, dupe, double-cross, hate, deception, joke, hoax, bluff, setup, prank, ruse, ploy, tactic, subterfuge, dishonesty, cunning, deceit, guile, sly, pretend, trap, not honest, no honest, no good, Test, @':;¬!"£%^&*()_+][#~}{
              </textarea>
            </div>
            <div class="form-group">
              <label for="">Comment Sentence</label>
              <textarea class="form-control description" style="height: 100px">
this website is not authentic, i do not like this site,
              </textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
@stop