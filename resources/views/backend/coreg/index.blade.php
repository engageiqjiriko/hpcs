@extends('backend.layouts.main' ,['title' => 'survey.coreg.list'])
@section('content')
@section('title', 'Coreg List')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Coreg List</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Coreg List</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-comment-o"></i>
        <span class="caption-subject bold uppercase"> Coreg List</span>
      </div>
      <div class="actions">
      	<a href="/add-feedback" class="btn btn-default">
      	 <i class="fa fa-plus"></i>	Add Coreg
      	</a>
      </div>
    </div>
    <div class="portlet-body">
    	<table class="table table-hover">
    		<thead>
    			<tr>
    				<th>Sr.#</th>
    				<th>User</th>
    				<th>Message</th>
    				<th>Status</th>
    				<th>Date Created</th>
    				<th>Actions</th>
    			</tr>
    		</thead>
    		<tbody>
    			@for($i = 1; $i<=10; $i++)
    				<tr>
    					<td>{{ $i }}</td>
    					<td>Vanessa <br/> vanessaarchange@yahoo.com</td>
    					<td style="width: 30%">The Surveys were very topical and seemed to be very informed. These surveys seemed to be given out at random but that makes it more fun right?</td>
    					<td><span class="label label-sm label-info"> Waiting for review </span></td>
    					<td>4 days ago</td>
    					<td></td>
    				</tr>
    			@endfor
    		</tbody>
    	</table>
    	<div class="text-right">
			  <ul class="pagination pagination-sm">
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-left"></i>
			      </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 1 </a>
			    </li>
			    <li class="active">
			      <a href="javascript:;"> 2 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 3 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 4 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 5 </a>
			    </li>
			    <li>
			      <a href="javascript:;"> 6 </a>
			    </li>
			    <li>
			      <a href="javascript:;">
			      <i class="fa fa-angle-right"></i>
			      </a>
			    </li>
			  </ul>
			</div>
    </div>
   </div>
	</div>
</div>
<!-- END MAIN CONTENT -->
@stop