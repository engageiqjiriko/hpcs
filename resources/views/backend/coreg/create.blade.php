@extends('backend.layouts.main' ,['title' => 'survey.coreg.create'])
@section('content')
@section('title', 'Create Coreg')
<!-- BEGIN BREADCRUMBS -->
<div class="breadcrumbs">
  <h1>Coregs</h1>
  <ol class="breadcrumb">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li class="active">Create Coreg</li>
  </ol>
</div>
<!-- END BREADCRUMBS -->
<!-- BEGIN MAIN CONTENT -->
<div class="row" id="app">
	<div class="col-md-12">
		<div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-comment-o"></i>
              <span class="caption-subject bold uppercase"> Create Coreg</span>
            </div>
          </div>
          <div class="portlet-body">

              <form id="coreg-form">
                    {{ csrf_field() }}
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Details</h4>
                      </div>
                  </div>
                      <div class="row form-group">
                          <div class="col-md-4">
                              <label for="">Name</label>
                              <input name="name" type="text" class="form-control">
                          </div>

                          <div class="col-md-4">
                              <label for="">Deal</label>
                              <input name="deal" type="text" class="form-control">
                          </div>

                          <div class="col-md-4">
                              <label for="">Description</label>
                              <textarea name="description" type="text" class="form-control" rows="3" style="resize: vertical"> </textarea>
                          </div>
                      </div>
                  <hr />
                  <div class="row">
                      <div class="col-md-12">
                          <h4>Required Fields</h4>
                      </div>
                  </div>
                      <div class="row form-group">
                          <div class="col-md-8">
                              <div class="row">
                                  <div class="col-md-4">
                                      <label for="">EIQ Campaign ID</label>
                                  </div>
                                  <div class="col-md-7">
                                      <label for="">Choose Required Fields</label>
                                  </div>
                                  <div class="col-md-1">
                                      <label for="">Action</label>
                                  </div>
                              </div>

                              <div class="row">
                                  <div class="col-md-4">
                                      <input name="eiq_campaign_id" type="number" class="form-control">
                                  </div>
                                  <div class="col-md-7">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <select v-model="fieldsSelected" name="" class="form-control" id="selectFields" :disable="!fields">
                                                  <option v-for="field in fields" v-if="field.status==0" :value="field.index">@{{ field.name }}</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12" v-if="fieldsSelected==39">
                                              <hr>
                                              <label for="">Custom Field</label>:<input type="text" v-model="customField">
                                              &nbsp;&nbsp;&nbsp;
                                              <label for="">Static Value</label>:<input type="text" v-model="customStaticValue">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-1">
                                      <button type="button" class="btn btn-primary" @click="fieldPusher">
                                      >>
                                      </button>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                              <div class="row">
                                  <div class="col-md-12">
                                      <label for="">Choosen required fields</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-12">
                                      <ul v-for="field in fields" v-if="field.status==1">
                                          <li>@{{ field.name }} <span class="pull-right"><button type="button" @click="fieldRemover(field.index)" class="btn btn-xs btn-danger">X</button></span></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input type="checkbox" value="true" v-model="additionalFlag"><label for="">You need additional custom fields?</label>
                            </div>
                        </div>

                        <div class="row form-group" v-if="additionalFlag">
                            <div class="col-md-12">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Additional Fields</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <label for="">Label Question</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="">Field Name</label>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Field Type</label>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="">Action</label>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Additional Field list</label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <textarea v-model="additionalLabel" class="form-control" name="" id="" style="resize: vertical" rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <input v-model="additionalField" type="text" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <select v-model="additionalType" class="form-control">
                                                            <option value="input">Input Field</option>
                                                            <option value="textarea">Text Area Field</option>
                                                            <option value="select">Selection Field</option>
                                                            <option value="checkbox">Checkbox with consent</option>
                                                            <option value="checkbox_x">Checkbox auto approved</option>
                                                            <option value="telephone">Phone Number</option>
                                                            <option value="mobile-phone">Mobile Phone</option>
                                                            <option value="phone-3">Phone Number(3 inputs)</option>
                                                            <option value="mobile-phone-3">Mobile Phone (3 inputs)</option>
                                                            <option value="yesOrNo">yes or no</option>
                                                            <option value="user_agent">User Agent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <hr>
                                                    </div>
                                                </div>

                                                <div class="row" v-show="additionalType=='select'">
                                                    <div class="col-md-12">
                                                        <h4>Create Options</h4>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input v-model="additionalOptionValue" type="text" class="form-control" placeholder="Value">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input v-model="additionalOptionField" type="text" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="button" @click="additionalOptionPusher" class="btn btn-primary form-control"> Insert this option </button>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <ul>
                                                            <li v-for="option in additionalOptions">@{{ option.name }}</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="row" v-show="additionalType=='checkbox'||additionalType=='checkbox_x'">
                                                    <div class="col-md-12">
                                                        <h4>Label for checkbox</h4>
                                                        <textarea v-model="additionalCheckLabel" name="" id=""  rows="3" class="form-control" style="resize: vertical;"></textarea>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-1">
                                                <button @click="additionalFieldPusher" type="button" class="btn btn-primary" @click="fieldPusher">
                                                >>
                                                </button>
                                            </div>
                                            <div class="col-md-3">
                                                <ul>
                                                    <li v-for="field in additionalFields" track-by="$index"> @{{ field.field }} - @{{ field.type }}</li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

              </form>
          </div>
      </div>
	</div>
</div>

<div class="portlet box border-dashed">
  <div class="portlet-body">
      <button type="button" class="btn btn-lg blue" @click="saveCoreg">
        <i class="fa fa-save"></i>  Save
      </button>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
@stop

@section('scripts')

    <script type="text/javascript">

        new Vue ({

            el: 'body',

            data: {

                fieldsSelected : 2,
                customField : null,
                customStaticValue : null,
                additionalLabel : null,
                additionalDescription: null,
                additionalType : 'input',
                additionalField : null,
                additionalFields : [],
                additionalOptionField : null,
                additionalOptionValue :null,
                additionalOptions : [],
                additionalCheckLabel : null,
                additionalFlag : false,
                fields :
                [
                        // all values equivalent here is a function in mapper.php
                    {index:0,field:'first_name', name:'first_name',value:'fname',status:1,refresh:1, custom:0},
                    {index:1,field:'last_name', name:'last_name',value:'lname',status:1,refresh:1, custom:0},
                    {index:2,field:'ip', name:'ip',value:'ip',status:0,refresh:0, custom:0},
                    {index:3,field:'zip', name:'zip',value:'zip',status:0,refresh:0, custom:0},
                    {index:4,field:'state', name:'state',value:'state',status:0,refresh:0, custom:0},
                    {index:5,field:'birth_date', name:'birth_date[yyyy-mm-dd]',value:'birthdate1',status:0,refresh:0, custom:0},
                    {index:6,field:'date_of_birth', name:'date_of_birth[VALUE_DOBMONTH]/[VALUE_DOBDAY]/[VALUE_DOBYEAR]',value:'birthdate2',status:0,refresh:0, custom:0},
                    {index:7,field:'dobday', name:'dobday[dd]',value:'dobday',status:0,refresh:0, custom:0},
                    {index:8,field:'dobmonth', name:'dobmonth[mm]',value:'dobmonth',status:0,refresh:0, custom:0},
                    {index:9,field:'dobyear', name:'dobyear[yyyy]',value:'dobyear',status:0,refresh:0, custom:0},
                    {index:10,field:'source', name:'source',value:'gy',status:0,refresh:0, custom:0},
                    {index:11,field:'gender', name:'gender',value:'gender',status:0,refresh:0, custom:0},
                    {index:12,field:'createDate', name:'createDate',value:'createDate',status:0,refresh:0, custom:0},
                    {index:13,field:'timestamp', name:'timestamp[DATE_TIME]',value:'timestamp1',status:0,refresh:0, custom:0},
                    {index:14,field:'datetime', name:'datetime[m/d/Y H:i:s]',value:'datetime1',status:0,refresh:0, custom:0},
                    {index:15,field:'datetime', name:'datetime[m-d-Y H:i:s]',value:'datetime2',status:0,refresh:0, custom:0},
                    {index:16,field:'title', name:'title[Mr/Mrs]',value:'title',status:0,refresh:0, custom:0},
                    {index:17,field:'address', name:'address',value:'address',status:0,refresh:0, custom:0},
                    {index:18,field:'phone', name:'phone',value:'phone',status:0,refresh:0, custom:0},
                    {index:19,field:'dob', name:'dob',value:'dob',status:0,refresh:0, custom:0},
                    {index:20,field:'add_code_padding', name:'add_code_padding',value:'addCodePadding',status:0,refresh:0, custom:0},
                    {index:21,field:'website', name:'website',value:'website',status:0,refresh:0, custom:0},
                    {index:22,field:'custom1', name:'custom1',value:'custom1',status:0,refresh:0, custom:0},
                    {index:23,field:'tpid', name:'tpid',value:'tpid',status:0,refresh:0, custom:0},
                    {index:24,field:'city', name:'city',value:'city',status:0,refresh:0, custom:0},
                    {index:25,field:'p_value', name:'p_value[CD[REV_TRACKER]]',value:'pvalue',status:0,refresh:0, custom:0},
                    {index:26,field:'rev_tracker_map', name:'rev_tracker_map',value:'revTrackerMap',status:0,refresh:0, custom:0},
                    {index:27,field:'today', name:'today',value:'today',status:0 ,refresh:0, custom:0},
                    {index:28,field:'dob', name:'dob',value:'dob',status:0 ,refresh:0, custom:0},
                    {index:29,field:'add_code', name:'add_code',value:'addCode',status:0 ,refresh:0, custom:0},
                    {index:30,field:'lr', name:'lr',value:'lr',status:0 ,refresh:0, custom:0},
                    {index:31,field:'age', name:'age',value:'age',status:0 ,refresh:0, custom:0},
                    {index:32,field:'postedstamp', name:'postedstamp',value:'postedstamp',status:0 ,refresh:0, custom:0},
                    {index:33,field:'dj', name:'dj',value:'dj',status:0 ,refresh:0, custom:0},
                    {index:34,field:'program_name', name:'program_name',value:'programName',status:0 ,refresh:0, custom:0},
                    {index:35,field:'program_id', name:'program_id',value:'programId',status:0 ,refresh:0, custom:0},
                    {index:36,field:'leadid', name:'leadid',value:'leadid',status:0 ,refresh:0, custom:0},
                    {index:37,field:'dob', name:'dob[VALUE_DOBMONTH]/[VALUE_DOBDAY]/[VALUE_DOBYEAR]',value:'birthdate2',status:0 ,refresh:0, custom:0},
                    {index:38,field:'debt_amount', name:'debt_amount',value:'debtAmount',status:0 ,refresh:0, custom:0},
                    {index:39,field:'custom', name:'CUSTOM + STATIC',value:'custom',status:0 ,refresh:0, custom:0},
                    {index:40,field:'date', name:'date',value:'today',status:0 ,refresh:0, custom:0}
                ]
            },

            ready : function(){

            },

            methods : {
                fieldPusher : function(){

                    var index = $('#selectFields').val();


                    if(index==39)
                    {
                        var custom = this.fields.length+1;
                        this.fields.push({index:custom,field:this.customField, name:this.customField,value:'static_'+this.customStaticValue,status:1 ,refresh:0, custom:1});


                        console.log(this.fields);
                        this.customField = null;
                        this.customStaticValue = null;
                        return false;
                    }
                    else if(index==null)
                    {
                        $('#selectFields').val('');
                    }

                    else if(index)
                    {
                        this.fields[index]['status']=1;
                        return false;
                    }
//
                },

                fieldRemover : function (fieldName) {
                    this.fields[fieldName]['status']=0;
                },

                additionalFieldPusher : function(){

                    var field = this.additionalField;
                    var label = this.additionalLabel;
                    var type = this.additionalType;

                    var options = null;


                    if(type=='select')
                    {
                        options = this.additionalOptions;
                    }
                    else if(type=='checkbox')
                    {
                        options = this.additionalCheckLabel;
                    }
                    else if(type=='yesOrNo')
                    {
                        this.additionalOptions.push({name:'yes',value:'yes'},{name:'no',value:'no'});
                        options = this.additionalOptions;
                        type = 'select';
                    }

                    this.additionalFields.push({field:field, type:type, description:label,options:options,status:1,required:1});

                    this.additionalCheckLabel = null;
                    this.additionalOptions = [];
                    this.additionalType = 'input';
                    this.additionalLabel = null;
                    this.additionalOptionField = null;
                },


                additionalOptionPusher : function(){
                    var field = this.additionalOptionField;
                    var value = this.additionalOptionValue;
                    this.additionalOptions.push({name:field,value:value});
                },

                saveCoreg : function(){

                    var self = this;
                    var tempo = this.fields;
                    var fields = [];



                    for(var i in tempo)
                    {
                        if(tempo[i]['status']==1)
                        {
                            fields.push({ key:tempo[i]['field'],value:tempo[i]['value'] });
                        }
                    }



                    var data = $('#coreg-form').serializeArray();
                    data.push({ name: 'fields', value: JSON.stringify(fields)});

                    if(this.additionalFlag==true)
                    {
                        data.push({ name: 'additional_fields', value: JSON.stringify(this.additionalFields)});
                    }

                    $.ajax({
                        url:'/admin/coreg/store',
                        method:'POST',
                        data: data
                    }).success(function(response) {
                        notify("Success!", "Campaign has been saved", "success");
                        $('#coreg-form').find("input[type=text], textarea, input[type=number]").val("");

                        self.resetFields();

                        console.log(response)
                    }).fail(function(e) {
                        notify(e.statusText || ($.parseJSON(e.responseText)).message, "", "error", 'overlay');
                    })
                },

                resetFields : function(){

                    var fields = this.fields;
                    for(var i in fields)
                    {
                        if(fields[i]['custom']==1)
                        {
                            fields.splice(i, 1);
                        }
                        else if(fields[i]['refresh']==0)
                        {
                            fields[i]['status']=0;
                        }
                    }
                    this.fieldsSelected=2;
                    this.fields = fields;

                    this.additionalFields=[];
                    this.additionalOptionField=null;
                    this.additionalOptions=[];
                    this.additionalCheckLabel=null;
                    this.additionalFlag=false;

                }
            }
        });

    </script>

@endsection



