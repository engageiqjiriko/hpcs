<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="false" data-slide-speed="200">
        <li @click="loadingPage" class="nav-item {{ $title=='Dashboard'? 'active' : '' }}">
            <a href="{{ route('admin.dashboard') }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li @click="loadingPage" class="nav-item  {{ $title=='Survey'? 'active' : '' }}">
            <a href="{{ route('admin.survey') }}" class="nav-link nav-toggle">
                <i class="icon-list"></i>
                <span class="title">Surveys</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li @click="loadingPage" class="nav-item  {{ $title=='Survey Question'? 'active' : '' }}">
            <a href="{{ route('admin.survey.questions') }}" class="nav-link nav-toggle">
                <i class="icon-question"></i>
                <span class="title">Survey Question</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li @click="loadingPage" class="nav-item  {{ $title=='Coreg'? 'active' : '' }}">
            <a href="{{ route('admin.coregs') }}" class="nav-link nav-toggle">
                <i class="icon-layers"></i>
                <span class="title">Coregs Campaign</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li @click="loadingPage" class="nav-item  {{ $title=='Local'? 'active' : '' }}">
            <a href="{{ route('admin.local') }}" class="nav-link nav-toggle">
                <i class="icon-link"></i>
                <span class="title">Local Surveys</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li @click="loadingPage" class="nav-item  {{ $title=='Users'? 'active' : '' }}">
            <a href="{{ route('admin.users') }}" class="nav-link nav-toggle">
                <i class="icon-people"></i>
                <span class="title">Users</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>

        <li @click="loadingPage" class="nav-item  {{ $title=='Feedback'? 'active' : '' }}">
        <a href="{{ route('admin.feedback') }}" class="nav-link nav-toggle">
            <i class="icon-bubbles"></i>
            <span class="title">Feedback & Opinions</span>
            <span class="selected"></span>
            <span class="arrow open"></span>
        </a>
        </li>

        <li @click="loadingPage" class="nav-item  {{ $title=='Zipcodes'? 'active' : '' }}">
            <a href="{{ route('admin.zipcodes') }}" class="nav-link nav-toggle">
                <i class="icon-puzzle"></i>
                <span class="title">Zip Codes</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>

        <li @click="loadingPage" class="nav-item  {{ $title=='Reports'? 'active' : '' }}">
            <a href="{{ route('admin.reports') }}" class="nav-link nav-toggle">
                <i class="icon-briefcase"></i>
                <span class="title">Report & Logs</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>

        {{--<li @click="loadingPage" class="nav-item  {{ $title=='Bad Comments'? 'active' : '' }}">--}}
            {{--<a href="{{ route('admin.bad.comments') }}" class="nav-link nav-toggle">--}}
                {{--<i class="icon-docs"></i>--}}
                {{--<span class="title">Bad Comments</span>--}}
                {{--<span class="selected"></span>--}}
                {{--<span class="arrow open"></span>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li @click="loadingPage" class="nav-item  {{ $title=='Bad Comments Filter'? 'active' : '' }}">--}}
            {{--<a href="{{ route('admin.bad.comments.filter') }}" class="nav-link nav-toggle">--}}
                {{--<i class="icon-tag"></i>--}}
                {{--<span class="title">Bad Comments Filter</span>--}}
                {{--<span class="selected"></span>--}}
                {{--<span class="arrow open"></span>--}}
            {{--</a>--}}
        {{--</li>--}}
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->