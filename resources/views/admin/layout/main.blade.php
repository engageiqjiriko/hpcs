<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>HPCS | @yield('title')</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="_token" content="{{ csrf_token() }}">

	<!-- Begin mandatory styles -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css">
	<!-- End mandatory styles -->
	<!-- BEGIN GLOBAL STYLES -->
	<link rel="stylesheet" type="text/css" href="/assets/admin/css/component.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/admin/css/plugin.min.css">
	<link rel="stylesheet" type="text/css" href="https://vadimsva.github.io/waitMe/waitMe.min.css">
	<!-- END GLOBAL STYLES -->
	<!-- BEGIN MAIN LAYOUT STYLES -->
	<link rel="stylesheet" type="text/css" href="/assets/admin/css/layout.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/admin/css/style.min.css">

	<link rel="stylesheet" type="text/css" href="/css/datepicker.css">
	<!-- END MAIN LAYOUT STYLES -->
	@yield('styles')
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
	@include('admin.layout.header')

	<div class="page-content-wrapper" id="page-loading">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content" id="page-wrapper">
			@include('admin._partial.breadcrumbs')
			<div class="row">
				<div class="col-md-12" id="page-content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	@include('admin.layout.footer')
	<!-- BEGIN CORE PLUGINS -->
	<script src="/assets/admin/js/jquery.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/cookie.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/slimscroll.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/blockui.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="/assets/admin/js/app.min.js" type="text/javascript"></script>
	<script src="https://vadimsva.github.io/waitMe/waitMe.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<!-- <script src="/assets/admin/js/dashboard.min.js" type="text/javascript"></script> -->
	<!-- END PAGE LEVEL SCRIPTS -->
	 <!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="/assets/admin/js/layout.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/demo.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/quick-sidebar.min.js" type="text/javascript"></script>
	<script src="/assets/admin/js/quick-nav.min.js" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.4/moment-timezone-with-data.min.js"></script>

	<script src="/js/backend/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

	<script>
		function startLoading() {
			$('#page-loading').waitMe({
				effect : 'rotateplane',
				text : 'Please wait',
				bg : 'rgba(255,255,255,0.1)',
				color : '#000'
			});
		}
		function stopLoading() {
			$('#page-loading').waitMe("hide");
		}
	</script>

	@stack('scripts')

	<script src="/js/backend/common.js"></script>
	<script src="{{ elixir('js/backend/main.js') }}"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>