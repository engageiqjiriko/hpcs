@extends('admin.layout.main', ['title' => 'Coreg','description' => 'Internal Coregs Campaign'])
@section('title', 'Dashboard')
@section('content')


    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-link font-dark"></i>
                <span class="caption-subject font-dark bold uppercase">Coregs Campaign</span>
            </div>
            <div class="actions col-md-3">
                <div class="input-group pull-right">
                    <input type="text" class="form-control" placeholder="Search for..."
                    v-model="searchField"
                    @keyUp="searchFor('coregSearch',$event) | debounce 700"
                    >
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <coregs></coregs>
        </div>
    </div>

@stop