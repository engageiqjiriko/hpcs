@extends('admin.layout.main', ['title' => 'Profile','description' => 'Administrator Profile'])
@section('title', 'Profile')
@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/admin/css/profile.min.css">
@stop
@section('content')
    <div class="row">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="/assets/admin/images/incognito.jpg" class="img-responsive" alt=""> </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }} </div>
                        <div class="profile-usertitle-job"> Site Administrator </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <!-- <button type="button" class="btn btn-circle green btn-sm">Follow</button> -->
                        <button type="button" class="btn btn-circle red btn-sm">Edit</button>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <!-- <ul class="nav">
                            <li class="active">
                                <a href="page_user_profile_1.html">
                                    <i class="icon-home"></i> Overview </a>
                            </li>
                            <li>
                                <a href="page_user_profile_1_account.html">
                                    <i class="icon-settings"></i> Account Settings </a>
                            </li>
                            <li>
                                <a href="page_user_profile_1_help.html">
                                    <i class="icon-info"></i> Help </a>
                            </li>
                        </ul> -->
                    </div>
                    <!-- END MENU -->
                </div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="alert alert-info">
		  		Welcome {{ \Auth::user()->first_name }}!
			</div>
			<div class="portlet light">
                <h4>User Information</h4>
                <hr>
                <form class="form-horizontal" role="form" action="{{ route('admin.profile.update') }}" method="post">
                            <div class="portlet light">
                                <div class="form-group">
                                    {{ csrf_field() }}
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control" value="{{ \Auth::user()->email }}" required> </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" name="password" class="form-control" placeholder="New Password" required> </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" required> </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Firstname</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{ \Auth::user()->first_name }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Lastname</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="{{ \Auth::user()->last_name }}" required> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" name="address" class="form-control" value="test" required> </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <br>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                </form>
			</div>
		</div>

	</div>

@stop