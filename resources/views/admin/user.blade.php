@extends('admin.layout.main', ['title' => 'Users','description' => 'HPCS / OLDHPCS / PFR Users'])
@section('title', 'Users')
@section('content')

    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="row">
                <div class="caption col-md-9"><label for=""><i class="fa fa-database" aria-hidden="true"></i> Database</label>
                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    <select name="" id="" v-model="dbMode">
                        <option value="HPCS">HPCS</option>
                        <option value="OLDHPCS">OLD HPCS</option>
                        <option value="PFR">PFR</option>
                    </select>
                    <p>
                        <i class="icon-people font-dark"></i>
                        <span class="caption-subject font-dark bold uppercase">Users</span>
                    </p>
                </div>
                <div class="actions col-md-3">
                    <div class="input-group pull-right">
                        <input v-model="searchField" @keyUp="searchFor('userSearch',$event) | debounce 1000" type="text" class="form-control" placeholder="Search for email">
                                    <span class="input-group-btn">
                                        <button @click="searchFor('userSearch',$event)" class="btn btn-default" type="button">Go!</button>
                                    </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <users :db-mode="dbMode"></users>
        </div>
    </div>

@stop
