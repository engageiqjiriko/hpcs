@section('title', 'Users')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit ">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-people font-dark"></i>
					<span class="caption-subject font-dark bold uppercase">{{ $title }}</span>
				</div>
				<div class="actions col-md-3">
					<div class="input-group pull-right">
						<input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				@yield('content')
			</div>
		</div>
	</div>
</div>