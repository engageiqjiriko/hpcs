<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
    <head>
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="_csrf" content="{{ csrf_token() }}">

        <!-- Begin Favicons for html and mobile browsers -->
        {{--<link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-touch-icon.png">--}}
        <link rel="icon" type="image/png" href="/images/favicons/favicon-32x32.png" sizes="32x32">
        {{--<link rel="icon" type="image/png" href="/images/favicons/favicon-16x16.png" sizes="16x16">--}}
        <link rel="manifest" href="/manifest.json">

        {{--<link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#5bbad5">--}}
        {{--<meta name="apple-mobile-web-app-title" content="HighPayingCashSurveys">--}}
        <meta name="application-name" content="HighPayingCashSurveys">
        <meta name="theme-color" content="#ffffff">
        <!-- End Favicons for html and mobile browsers -->

        <meta property="og:url"           content="{{ url('/') }}" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="High Paying Cash Surveys" />
        <meta property="og:description"   content="Market research companies pay us to offer you with surveys for your opinions. They pay us, you get to take surveys for free. Even better, you can get rewarded with cash, prizes and products. This is not a scheme this is a way to make a little extra cash. We have peers writing reviews and rating surveys to help you to make a decision on what surveys to do after you perform a local search on our site today!" />
        <meta property="og:image"         content="{{ url('/') }}/images/logo.png" />


        <title>High Paying Cash Surveys</title>

        {{--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">--}}
        <link rel="stylesheet" href="{{ elixir('css/app.css') }}" id="main">
        <!-- Fontawesom icons -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        @yield('links')
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Hotjar Tracking Code for test.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:406576,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        {{--pushcrew notifcation--}}
        {{--<script type="text/javascript">--}}
            {{--(function(p,u,s,h){--}}
                {{--p._pcq=p._pcq||[];--}}
                {{--p._pcq.push(['_currentTime',Date.now()]);--}}
                {{--s=u.createElement('script');--}}
                {{--s.type='text/javascript';--}}
                {{--s.async=true;--}}
                {{--s.src='https://cdn.pushcrew.com/js/b3e38f443545d70e6179a2f17c608bf6.js';--}}
                {{--h=u.getElementsByTagName('script')[0];--}}
                {{--h.parentNode.insertBefore(s,h);--}}
            {{--})(window,document);--}}
        {{--</script>--}}


        {{-- Google Recaptcha --}}
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>
    <body v-cloak id="bodyOnClick" tabindex="1">
        <!-- Static navbar -->
        <nav class="navbar navbar-default hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <!-- <ul class="nav navbar-nav">
                            <li class="hidden-md hidden-lg hidden-sm"><a href="javascript:;" @click="mobileWallet()">My Wallet</a></li>
                        </ul> -->
                    </div>
                    <div class="col-xs-6">
                        <ul class="nav navbar-nav navbar-right text-right" id="navigation">
                            @if(!$loggedUser)
                                <li><a href="javascript:;" @click="loginModal()">Login</a></li>
                            @endif
                            @if($loggedUser)
                                <li>
                                    <a href="/logout">Logout</a>
                                    {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi! {{ $loggedUser->first_name }} <i class="fa fa-caret-down" aria-hidden="true"></i></a>--}}
                                    {{--<div class="dropdown-menu">--}}
                                        {{--<p>hehe</p>--}}
                                        {{--<p>haha</p>--}}
                                    {{--</div>--}}
                                </li>


                                {{--<li class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--MY name--}}
                                    {{--<div class="btn-group">--}}
                                        {{--<div class="dropdown-menu">--}}
                                            {{--...--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                            @endif
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="wallet-earning"></div>
                </div>
            </div>
        </nav>
        <div class="landscape text-center">
            <img src="/images/phone-rotate.gif" class="img-responsive rotate">
            <div class="alert-container">
                <h4>Sorry, landscape mode is not supported. Please use portrait instead.</h4>
            </div>
        </div>

        @yield('content')
        <!-- Modal -->
        <div id="login-modal" class="modal fade" role="dialog">
            <Login></Login>
        </div>

        <modal-faq></modal-faq>
        <modal-privacy></modal-privacy>
        <modal-terms></modal-terms>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="/js/plugins.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.4/moment-timezone-with-data.min.js"></script>

        {{--social src--}}

        {{--<script src="https://apis.google.com/js/platform.js"></script>--}}

        {{--<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>--}}
        {{--<script type="IN/FollowCompany" data-id="116221" data-counter="right"></script>--}}

        {{--<script async defer src="//assets.pinterest.com/js/pinit.js"></script>--}}

        {{--end here--}}

        <script>
            {{--var env = "{{ env }}";--}}
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-88397146-1', 'auto');



        </script>



        <script src="/js/backend/common.js"></script>
        <script src="{{ elixir('js/frontend/main.js') }}"></script>
        @yield('scripts')
        @include('flash::message')
        <script>
            function swapStyleSheet(sheet){
                var oldSrc = '/images/promise.png';
                var newSrc = '/images/promise2.png';

                document.getElementById('main').setAttribute('href', sheet);
                $('#addstyle').remove();
                $('.floater-container').append('<button id="revert" type="button" style="background: #BABCBE !important;" class="floater btn btn-success" onClick="window.location.reload()" title="Click to change theme"><i class="fa fa-refresh"></i></button>');
                $('img[src="' + oldSrc + '"]').attr('src', newSrc).css({'width':'300px', 'float':'right'});
            }

            (function() {
                        var field = 'xxTrustedFormCertUrl';
                        var provideReferrer = false;
                        var tf = document.createElement('script');
                        tf.type = 'text/javascript'; tf.async = true;
                        tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +
                                '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random();
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }
            )();

        </script>

    </body>
</html>