 <!-- Accordion -->
<div class="panel-group accordion footer-accordion  privacy-accordion animated" style="display:none" transition="bounce" v-show="policy">
    <div class="panel panel-default">
        <button type="button" class="btn btn-privacy" @click="policy=!policy">Privacy Policy</button>
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapsePolicy"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapsePolicy">Our Policy</a>
            </h4>
        </div>
        <div id="collapsePolicy" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    The Company is strongly committed to protecting the privacy of its user community. The intent of this privacy policy ("Privacy Policy") is to detail the information the Company might gather about individuals who visit Highpayingcashsurveys.com Sites, how that information is used, and our disclosure practices. Please note that this Privacy Policy applies only to websites owned and operated by the Company, and not to the sites of other companies or organizations to which we provide links. By using any Highpayingcashsurveys.com Sites, you signify that you have read, understand and agree to be bound by this Privacy Policy as well as our Terms of Use, which are incorporated by reference herein to this Privacy Policy. 

                    Typically, we will seek consent for the collection, use or disclosure of your personal information at the time of collection. In certain circumstances, consent may be sought after the information has been collected but before use (for example, when we want to use information for a purpose not previously identified). The form of consent that we seek, including whether it is express or implied, will largely depend on the sensitivity of the personal information and the reasonable expectations of the individual in the circumstances. 

                    You may withdraw consent at any time, subject to legal or contractual restrictions and reasonable notice. If you wish to withdraw your consent at any time, please contact us as set out below. We will inform you of the implications of withdrawing consent. 

                    We will not, as a condition of the supply of a product or service, require you to consent to the collection, use or disclosure of information beyond that required to fulfill the explicitly specified and legitimate purposes for which the information is being provided.
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseCollect"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCollect">
                What Personal Information We Collect? 
            </a>
            </h4>
        </div>
        <div id="collapseCollect" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    When you sign up for the Highpayingcashsurveys.com Sites you will be required to share information that identifies you personally. "Personal Information" refers to any information that lets us know the specifics of who you are and which can be used to identify, contact or locate you. Examples of Personal Information include, without limitation, your first name together with your last name, your mailing address and/or email address. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseInfo"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseInfo">
                When We Collect Personal Information? 
            </a>
            </h4>
        </div>
        <div id="collapseInfo" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    Generally, we collect Personal Information when you provide it to us by signing up on Highpayingcashsurveys.com Sites, filling out surveys. 

                    The Company usually collects Personal Information so that it can provide you with the Company's goods or services. The Company may also use your Personal Information for other reasons, including updating you on the Company's services, news and special offers (unless you choose to "opt-out" as described in the "Choice/Opt-out" section below). If you don't provide some of the Personal Information the Company asks for (e.g. your name, your mailing address and/or email address), it may be unable to provide you with the Company's goods or services. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseAnonymous"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAnonymous">
                What Anonymous User Information do we collect? 
            </a>
            </h4>
        </div>
        <div id="collapseAnonymous" class="panel-collapse collapse-one collapse">
            <div class="panel-body">
                <p>
                    The Company collects or may collect some information each time you visit Highpayingcashsurveys.com Sites so we can improve the overall quality of your online experience. We collect or may collect your IP address, referral data, and browser and platform type. You do not have to register on the Highpayingcashsurveys.com Sites or provide any Personal Information to us before we can collect this anonymous information. The Company assigns an anonymous ID number to your requests and links the following additional data to that anonymous ID number: the date and time you visited, your search terms, and the links you choose to click. Like most standard Web site servers, we use log files to collect and store this anonymous user information. The Company analyzes the information to determine trends, administer the site, track user's movement in the aggregate, and gather broad demographic information for aggregate use. Such aggregated anonymous information is used in our market research surveys, to record statistics behind special offers, our "shop & earn" promotions, electronic recycling and our polls. 

                    When you communicate with forms and highpayingcashsurvey.com support? If you contact the Company, we may keep a record of that correspondence and we may collect your email address for archival purposes but will not use, transfer or sell your email address except as set forth in this Privacy Policy. 

                    We have Third Party Advertisements and what you should know? The Highpayingcashsurveys.com Sites contain links to other sites on the Web. Please be aware that the Company is not responsible for the privacy practices of such other sites. We encourage our users to be aware when they leave the Highpayingcashsurveys.com Sites and to read the privacy statements of each and every Web site that collects personally identifiable information. This privacy policy applies solely to information collected by Highpayingcashsurveys.com Sites. 

                    The Highpayingcashsurveys.com Sites may contain links to various other sites that are not owned by the Company. While the Company makes every effort to ensure that its advertisers post clear and complete privacy policies and observe appropriate data practices, each of these sites has a privacy policy that may differ from that of the Company. The privacy practices of other websites and companies are not covered by this Privacy Policy. Please be aware that the Company is not responsible for the privacy practices of such other sites. The Company encourages you to be aware when you leave the Highpayingcashsurveys.com Sites and to read the privacy statements of each and every website that collect personally identifiable information. 

                    The Company may allow other companies, called third party ad servers or ad networks, to display advertisements on the Highpayingcashsurveys.com Sites or in Highpayingcashsurveys.com emails to you. Some of these ad networks may place a persistent cookie on your computer. Doing this allows them to recognize your computer each time they send you an online advertisement. In this way, ad networks may compile information about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. The Company does not have access to or control over the cookies that may be placed by these parties on your computer, and has no control over these parties' privacy policies or information collection practices. 

                    Except as specifically set forth in this Privacy Policy, the Company does not to provide your Personal Information to its advertisers unless you make the choice to share it with them. The Company occasionally passes Personally Identifiable Information via a secure interface to certain of its advertisers for the purpose of making it easier for you to sign up for their offers. However, this information is only to be used by the advertiser if you agree to submit the information to the advertiser for the purposes of completing an offer. 

                    In the event that the Company is unable to capture your transaction detail, the Company may need to validate your transaction with an advertiser or merchant partner to ensure that your account is credited. The validation information shared is used solely for this purpose and may include your first name, last name, email address, mailing address or date of birth. The Company will require that these third parties comply with the information practices set forth in this Privacy Policy and that the information not be shared or used for any other purpose. 

                    The Company may share non-personally identifiable information collected via the service in aggregate, anonymous form with advertisers or other third parties so that they may better evaluate what products and services are most appealing to different segments of the Company's user base. The Company does not disclose your first name, last name, address, email address, or any other Personal Information to these third parties unless you give your express consent. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseCookies"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCookies">
                Do we use online cookies?  
            </a>
            </h4>
        </div>
        <div id="collapseCookies" class="panel-collapse collapse-one collapse">
            <div class="panel-body">
                <p>
                    The Company may set and access cookies on your computer. A cookie is a small data file that a Web site may send to your browser and which may then be stored on your system. We use both session cookies and persistent cookies. For the session cookie, once you close the browser, the cookie terminates. A persistent cookie is a small text file stored on your hard drive for an extended period of time. Persistent cookies can be removed by following Internet browser help file directions. We use cookie and web beacon technology to track which pages on the Highpayingcashsurveys.com Sites our visitors view and which Web browsers our visitors use. We may also use cookies to help us provide personalized features and preferences to you within the various Highpayingcashsurveys.com Sites in a way that may be linked to individual user information that you elect to share with us. We also use "action tags" to assist in delivering the cookie. These action tags allow us to compile statistics and other data about our customers on an aggregate basis, by embedding a random identifying number on each user's browser. We then track where that number shows up on our site. 

                    We will never share such personally identifiable information with third parties except when required by law, where we have a good-faith belief that such action is necessary to comply with a judicial proceeding or administrative legal, a criminal investigation, a court order, or legal process served on Highpayingcashsurveys.com Sites. We may also share your Personal Information if we believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Highpayingcashsurveys.com Sites' terms of use, or as otherwise required by law. 

                    Personal Information collected by the Company may be disclosed to third parties to whom the Company contracts out specialized goods or services (such as IT service providers). If the Company does disclose Personal Information to third party contractors, the Company takes steps to ensure that those contractors:
                    
                    <ul>
                        <li>Comply with their obligations under applicable privacy laws; and </li>
                        <li>Are authorized only to use Personal Information in order to provide the goods and services required by the Company. </li>
                    </ul>

                    Most web browsers automatically accept cookies, but you can usually configure your browser to prevent this. Not accepting cookies may make certain features of the Highpayingcashsurveys.com Sites unavailable to you, though we will make every effort to ensure that users who have disabled cookies are denied access only to those portions of the website that legitimately require cookies for their functionality. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseMergers"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseMergers">
                What happens in the event of Mergers; Business Transitions?  
            </a>
            </h4>
        </div>
        <div id="collapseMergers" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    In the event the Company goes through a business transition, such as a merger, being acquired by another company, or selling a portion of its assets, users' Personal Information will, in most instances, be part of the assets transferred, unless such a transfer will violate any applicable law. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseChoice"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseChoice">
                Do not like us? You have the Choice/Opt-out.
            </a>
            </h4>
        </div>
        <div id="collapseChoice" class="panel-collapse collapse-one collapse">
            <div class="panel-body">
                <p>
                    Our goal is to keep you in control of the Personal Information you provide to us. Accordingly, you can review, correct, change or remove any personal registration information you provide to the Company. In order to "opt-out" and elect not to receive future communications from us, please email us at <a href="mailto:support@Highpayingcashsurveys.com" style="color:#56A4DA">support@Highpayingcashsurveys.com</a>. You can request access to, and correction of, your Personal Information by contacting the Company as follows:
                    <address>
                        Highpayingcashsurveys.com <br>
                        3080 Olcott street, 140 C. <br>
                        Santa Clara, Ca. <br>
                        <a href="mailto:help@highpayingcasurveys.com" style="color:#56A4DA">
                            help@highpayingcasurveys.com
                        </a>
                    </address>
                    Please note that as long as you remain a Highpayingcashsurveys.com user, opting not to receive email does not exempt you from receiving administrative emails, which include, but are not limited to, notices about vital service and status changes or alterations to the Company's Terms. 
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdate"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdate">
                If we update our privacy policy how it will be handled?
            </a>
            </h4>
        </div>
        <div id="collapseUpdate" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    We do reserve the right to make changes to this Privacy Policy from time to time. If we decide to change this Privacy Policy, we will post those changes in places on the Highpayingcashsurveys.com Sites deemed appropriate by us so our users are always aware of what information we collect, how we use it, and under what circumstances, if any, we would disclose it. If, however, we are going to use Personal Information in a manner different from that stated at the time of collection we will notify users by posting a notice on Highpayingcashsurveys.com Sites for at least 30 days. Your continued use of Highpayingcashsurveys.com Sites after the changes are posted constitutes your agreement to the changes. If you do not agree to the changes, please discontinue your use of Highpayingcashsurveys.com Sites.  
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapseChildren"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseChildren">
                Special Notification with respect to Children's Privacy (Users under the age of 13)
            </a>
            </h4>
        </div>
        <div id="collapseChildren" class="panel-collapse collapse-one collapse">
            <div class="panel-body">
                <p>
                    In response to concerns about protecting children's privacy online, Congress enacted the Children's Online Privacy Protection Act of 1998 ("COPPA"), which sets forth rules and procedures governing the ways in which Web sites may collect, use and disclose any personal information for children under the age of 13. In accordance with the Company policy and COPPA regulations, we DO NOT: 1. Request or knowingly collect personally identifiable information online or offline contact information from users under 13 years of age; or 2. Knowingly use or share personal information from users under 13 years of age with third parties; We request that children under the age of 13 not submit any Personal Information to us via the Highpayingcashsurveys.com Sites. It is possible that by fraud or deception we may receive information given to us or pertaining to children under 13. If we are notified of this, as soon as we verify the information, we will immediately delete the information from our servers. 

                    Questions regarding children's privacy should be directed to:

                    <address>
                        Highpayingcashsurveys.com <br>
                        3080 Olcott street, 140 C. <br>
                        Santa Clara, Ca. <br>
                        <a href="mailto:help@highpayingcasurveys.com" style="color:#56A4DA">
                            help@highpayingcasurveys.com
                        </a>
                    </address>
                </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
            <i class="indicator fa fa-plus-circle pull-left" aria-hidden="true" data-toggle="collapse" data-parent="#accordion" href="#collapsePersonal"></i>
            <a data-toggle="collapse" data-parent="#accordion" href="#collapsePersonal">
                Transfers of your Personal Information 
            </a>
            </h4>
        </div>
        <div id="collapsePersonal" class="panel-collapse collapse">
            <div class="panel-body">
                <p>
                    Although the Company houses its data in the United States, it is possible that some of data processing may occur outside of the United States. While the data protection laws of these countries may vary, the Company will make every reasonable effort to protect your information based upon the express terms of this Privacy Policy. By enrolling in the service, you consent to this transfer of your personal data to any server used by the Company.
                </p>
            </div>
        </div>
    </div>
</div> <!-- end #accordion -->