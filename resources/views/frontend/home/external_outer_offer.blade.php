@extends('frontend.layouts.main')
@section('content')

    <survey v-show="rendered" :device="{{ $device  }}" :prepop="{{ $prepop }}" :options="{{ $options }}"></survey>

@stop
@section('links')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
@stop