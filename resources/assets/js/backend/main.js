var Vue = require('vue');

var VueValidator = require('vue-validator');
Vue.use(VueValidator);

Vue.validator('email', function (val) {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
});

Vue.validator('number', function (val) {
    return /^[0-9]+$/.test(val)
});

import Users from './components/Users.vue';
import Dashboard from './components/Dashboard.vue';
import BadComments from './components/BadComments.vue';
import BadCommentsFilter from './components/BadCommentsFilter.vue';
import Feedback from './components/Feedback.vue';
import GoodComments from './components/GoodComments.vue';
import SurveyQuestions from './components/SurveyQuestions.vue';
import Survey from './components/Surveys.vue';
import ZipCodes from './components/Zipcodes.vue';
import Local from './components/Local.vue';
import Coregs from './components/Coregs.vue';
import Reports from './components/Reports.vue';

new Vue({
    el: 'body',
    data: {
        searchField : null,
        dbMode : 'HPCS'
    },
    components:
    {
        Users,
        Dashboard,
        BadComments,
        BadCommentsFilter,
        Feedback,
        GoodComments,
        SurveyQuestions,
        Survey,
        ZipCodes,
        Local,
        Coregs,
        Reports
    },
    methods: {
        loadingPage : function(e) {

            var $this = $(e.target);
            $('.nav-item').removeClass("active");

            $this.addClass("active");
            $('#page-wrapper').empty();
            startLoading();
        },

        searchFor : function (page,e)
        {
            // if (!(e.which !== 0 && e.charCode !== 0 && !e.ctrlKey && !e.metaKey && !e.altKey))
            // {
            //     return false;
            // }
            this.$broadcast(page,this.searchField);

        }
    }
});