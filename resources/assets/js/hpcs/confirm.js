function sendAjax(url, method, data) {
    return $.ajax({
        type: method,
        url: url,
        data: data
    })
}

export default class Confirm {
    static regular(url,method,callback,before = function(){})
    {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            before();
            sendAjax(url,method).success(function() {
                Notify.info('Deleted!');
                callback();
            }).error(function(e) {
                Notify.error(($.parseJSON(e.responseText)).message, "",true);
            });

        });
    }

    static delete(callback,params,msg ='You will not be able to recover this.')
    {
        swal({
            title: "Are you sure?",
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            callback(params);
            swal.close();
            Notify.info('Deleted!');
        });
    }
}