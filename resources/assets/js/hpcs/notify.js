export default class Notify {
    static info(title, message = '', overlay = false)  {
        swal({
            title: title,
            text: message,
            type: "info",
            timer: overlay ? null : 3000,
            showConfirmButton: overlay
        });
    }

    static success(title, message = '', overlay = false) {
        swal({
            title: title,
            text: message,
            type: "success",
            timer: overlay ? null : 3000,
            showConfirmButton: overlay
        });
    }

    static error(title, message = '', overlay = false) {
        swal({
            title: title,
            text: message,
            type: "error",
            timer: overlay ? null : 3000,
            showConfirmButton: overlay
        });
    }

    static warning(title, message = '', overlay = false) {
        swal({
            title: title,
            text: message,
            type: "warning",
            timer: overlay ? null : 3000,
            showConfirmButton: overlay
        });
    }
}