window._ = require('lodash');
window.Cookies = require('js-cookie');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass/assets/javascripts/bootstrap');

/**
 * Vue is a modern JavaScript for building interactive web interfaces using
 * reacting data binding and reusable components. Vue's API is clean and
 * simple, leaving you to focus only on building your next great idea.
 */

window.Vue = require('vue');
var VueValidator = require('vue-validator');

Vue.use(VueValidator);

window.Notify = require('./hpcs/notify');
window.Confirm = require('./hpcs/confirm');


//plugins
window.Taggle = require('taggle');
window.Dropzone = require('dropzone');
window.DataTable = require('datatables.net');
require('datatables.net-bs');
window.swal = require('sweetalert');



/**
 * We'll register a HTTP interceptor to attach the "XSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

$.ajaxSetup({
    headers: {
        'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
    }
});
