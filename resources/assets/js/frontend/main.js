var Vue = require('vue');


// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;


var VueValidator = require('vue-validator');
Vue.use(VueValidator);

Vue.transition('bounce', {
    enterClass: 'bounceInUp',
    leaveClass: 'fadeOut',
    type: 'animation'
});

Vue.transition('fade', {
    enterClass: 'fadeInLeft',
    leaveClass: 'fadeOutDown',
    type: 'animation'
});

Vue.transition('rubber', {
    enterClass: 'rubberBand',
    leaveClass: 'fadeOut',
    type: 'animation'
});


Vue.transition('wobble', {
    enterClass: 'fadeIn',
    leaveClass: 'fadeOut',
    type: 'animation'
});


Vue.transition('zoom', {
    enterClass: 'zoomInUp',
    leaveClass: 'fadeOutDown',
    type: 'animation'
});

Vue.transition('down', {
    enterClass: 'fadeInDown',
    leaveClass: 'fadeOutUp',
    type: 'animation'
});


import Survey from './components/Survey.vue';
import Login from './components/Login.vue';
import ModalFaq from './components/Footer/Modal/Faq.vue';
import ModalPrivacy from './components/Footer/Modal/Privacy.vue';
import ModalTerms from './components/Footer/Modal/Terms.vue';

Vue.validator('email', function (val) {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
});

new Vue({
    el: 'body',
    data: {
        //user: hpcs.user,
        faq:false,
        policy:false,
        terms:false,
        rendered : false
    },

    ready(){
        this.rendered = true;
    },

    components: { Survey, Login, ModalFaq, ModalPrivacy, ModalTerms },
    methods: {
        mobileWallet() {
            this.$broadcast('showMobilewallet');
        },
        showTerms() {
            this.terms = true;
            this.policy = false;
            this.faq = false;
        },
        showPolicy() {
            this.policy = true;
            this.faq = false;
            this.terms = false;
        },
        showFaq() {
            this.faq = true;
            this.policy = false;
            this.terms = false;
        },

        loginModal() {
            this.$broadcast('loginModal');
        }
    }
});

function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa-minus-circle fa-plus-circle');
}

$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_csrf"]').attr('content')
        }
    });

    $('.footer-accordion').on('hidden.bs.collapse', toggleIcon);
    $('.footer-accordion').on('shown.bs.collapse', toggleIcon);

    /*
     * this swallows backspace keys on any non-input element.
     * stops backspace -> back
     */
    var rx = /INPUT|SELECT|TEXTAREA/i;

    $(document).bind("keydown keypress", function(e){
        if( e.which == 8 ){ // 8 == backspace
            if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
                e.preventDefault();
            }
        }
    });
});



