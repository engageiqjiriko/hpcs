var LaravelElixirBundle = (function () {
	'use strict';

	function interopDefault(ex) {
		return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
	}

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var Notify$1 = function Notify () {};

	Notify$1.info = function info (title, message, overlay)  {
	        if ( message === void 0 ) message = '';
	        if ( overlay === void 0 ) overlay = false;

	    swal({
	        title: title,
	        text: message,
	        type: "info",
	        timer: overlay ? null : 3000,
	        showConfirmButton: overlay
	    });
	};

	Notify$1.success = function success (title, message, overlay) {
	        if ( message === void 0 ) message = '';
	        if ( overlay === void 0 ) overlay = false;

	    swal({
	        title: title,
	        text: message,
	        type: "success",
	        timer: overlay ? null : 3000,
	        showConfirmButton: overlay
	    });
	};

	Notify$1.error = function error (title, message, overlay) {
	        if ( message === void 0 ) message = '';
	        if ( overlay === void 0 ) overlay = false;

	    swal({
	        title: title,
	        text: message,
	        type: "error",
	        timer: overlay ? null : 3000,
	        showConfirmButton: overlay
	    });
	};

	Notify$1.warning = function warning (title, message, overlay) {
	        if ( message === void 0 ) message = '';
	        if ( overlay === void 0 ) overlay = false;

	    swal({
	        title: title,
	        text: message,
	        type: "warning",
	        timer: overlay ? null : 3000,
	        showConfirmButton: overlay
	    });
	};



	var require$$1 = Object.freeze({
	    default: Notify$1
	});

	function sendAjax(url, method, data) {
	    return $.ajax({
	        type: method,
	        url: url,
	        data: data
	    })
	}

	var Confirm = function Confirm () {};

	Confirm.regular = function regular (url,method,callback,before)
	{
	        if ( before === void 0 ) before = function(){};

	    swal({
	        title: "Are you sure?",
	        text: "You will not be able to recover this.",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, delete it!",
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	    }, function () {
	        before();
	        sendAjax(url,method).success(function() {
	            Notify.info('Deleted!');
	            callback();
	        }).error(function(e) {
	            console.log(e);
	            Notify.error(($.parseJSON(e.responseText)).message, "",true);
	        });

	    });
	};

	Confirm.delete = function delete$1 (callback,params,msg)
	{
	        if ( msg === void 0 ) msg ='You will not be able to recover this.';

	    swal({
	        title: "Are you sure?",
	        text: msg,
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Yes, do it!",
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	    }, function () {
	        callback(params);
	        swal.close();
	        Notify.info('Deleted!');
	    });
	};



	var require$$0 = Object.freeze({
	    default: Confirm
	});

	var utilities = createCommonjsModule(function (module) {
	var Notify = interopDefault(require$$1);
	var Confirm = interopDefault(require$$0);


	window.Notify = Notify;
	window.Confirm = Confirm;
	});

	var utilities$1 = interopDefault(utilities);

	return utilities$1;

}());

//# sourceMappingURL=utilities.js.map
