var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var settingForm = $('#page-setting');
            var errorSetting = $('.alert-danger', settingForm);
            var successSetting = $('.alert-success', settingForm);

            settingForm.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                   no_of_dummy_designations: {
                    required: true
                   },
                   no_of_coreg_designations: {
                    required: true
                   },
                   challenge_trigger: {
                    required: true
                   },
                   eiq_affiliate_id: {
                    required: true
                   },
                   rev_tracker: {
                    required: true
                   },
                   survey: {
                    required: true
                   }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    successSetting.hide();
                    errorSetting.show();
                    App.scrollTo(errorSetting, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    successSetting.show();
                    errorSetting.hide();
                    console.log("submitting form");
                }
            });


    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidation1();

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});